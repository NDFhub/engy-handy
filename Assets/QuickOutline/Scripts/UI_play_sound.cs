using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_play_sound : MonoBehaviour, IPointerEnterHandler
{
     private AudioSource _OnPointerEntersound;
     private AudioSource _ClickPointersound;

    private AudioSource _AudioSource;

    private GameObject _OnPointerEnter;
    private GameObject _ClickPointer;

    //private Button yourButton;

    private void Start()
    {
        Button btn = gameObject.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

        _OnPointerEnter = GameObject.Find("UIsound_OnPointerOver");
        _OnPointerEntersound = _OnPointerEnter.GetComponent<AudioSource>();
        _ClickPointer = GameObject.Find("UIsound_OnClickPointer");
        _ClickPointersound = _ClickPointer.GetComponent<AudioSource>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

        _OnPointerEntersound.Play();

        //Debug.Log("ONevent");
    }

    void TaskOnClick()
    {
        _ClickPointersound.Play();
    }



}
