using UnityEngine;
using System.Linq;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("UI")]
	public class ShowUIGroup : FsmStateAction
	{
		public UIComponent.Group group;

		// Code that runs on entering the state.
		public override void OnEnter()
		{
			set();
			Finish();
		}

		void set()
		{
			var s = GameObject.FindObjectsOfType<UIComponent>(true);
			//Debug.Log(s.Length);
			foreach (var ui in s)
			{
				var in_group = ui.groups.Contains(group);
				//Debug.Log(ui.id + ": " + in_group);
				if (in_group)
				{
					ui.Show();
				}
				else
                {
					ui.Hide();
                }
			}
		}

	}

}
