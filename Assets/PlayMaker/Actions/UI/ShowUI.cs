using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("UI")]
	public class ShowUI : FsmStateAction
	{
		public string ui;
		public bool show;

		public override void OnEnter()
		{
			Finish();
		}

		void set()
		{
			if (UIComponent.dict.ContainsKey(ui) == false)
			{
				UIComponent.NewCache();
			}

			if (UIComponent.dict.ContainsKey(ui))
			{
				UIComponent.dict[ui].Show(show);
			}
		}
	}
}
