using UnityEngine;
using System.Linq;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Tool")]
	public class EnableTool : FsmStateAction
	{
		public string tool;
		public bool enable;

		public override void OnEnter()
		{
			var list = GameObject.FindObjectsOfType<ToolObject>(true);
			var find = list.FirstOrDefault(x => x.id == tool);
			Debug.Log("find " + find.id);
			if (find)
			{
				find.gameObject.SetActive(enable);
			}

			Finish();
		}


	}

}
