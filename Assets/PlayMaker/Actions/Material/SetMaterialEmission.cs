using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory(ActionCategory.Material)]
	public class SetMaterialEmission : FsmStateAction
	{
		[RequiredField]
		[CheckForComponent(typeof(Renderer))]
		[Tooltip("A Game Object with a Renderer component.")]
		public FsmOwnerDefault gameObject;

		[Tooltip("The index of the material on the object.")]
		public FsmInt materialIndex;

		public bool ShowEmission;
		public Color color;

		string keyword = "_EMISSION";
		string keyword_color = "_EmissionColor";

		Renderer renderer;

		// Code that runs on entering the state.
		public override void OnEnter()
		{
			var go = Fsm.GetOwnerDefaultTarget(gameObject);		
			renderer = go.GetComponent<Renderer>();

			if (renderer)
            {
				if (materialIndex.Value == 0)
                {
					if (ShowEmission)
						renderer.material.EnableKeyword(keyword);
					else
						renderer.material.DisableKeyword(keyword);

					renderer.material.SetColor(keyword_color, color);
				}
				else
                {
					if (ShowEmission)
						renderer.materials[materialIndex.Value].EnableKeyword(keyword);
					else
						renderer.materials[materialIndex.Value].DisableKeyword(keyword);

					renderer.materials[materialIndex.Value].SetColor(keyword_color, color);
				}
			}

			Finish();
		}


	}

}
