using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory(ActionCategory.Material)]
	public class SetMaterials : FsmStateAction
	{
		public FsmOwnerDefault gameObject;
		public int leftCount = 1;

		// Code that runs on entering the state.
		public override void OnEnter()
		{
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			var renderer = go.GetComponent<Renderer>();

			var mats = renderer.materials;
			var new_mats = new Material[leftCount];
            for (int i = 0; i < leftCount; i++)
            {
				new_mats[i] = mats[i];
            }
			//new_mats[0] = mats[0];

			renderer.materials = new_mats;

			Finish();
		}


	}

}
