using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Game")]
	public class ShowResult : FsmStateAction
	{
		public override void OnEnter()
		{
			if (Main.singleton.selectGame.easy)
				Main.singleton.result.Show();
			else
				Main.singleton.NextItem();

			Finish();
		}
	}

}
