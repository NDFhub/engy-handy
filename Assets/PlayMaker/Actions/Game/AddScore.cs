using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Game")]
	public class AddScore : FsmStateAction
	{
		[RequiredField]
		[CheckForComponent(typeof(Score))]
		public FsmOwnerDefault gameobject;

		public override void OnEnter()
		{
			var go = Fsm.GetOwnerDefaultTarget(gameobject);
			var script = go.GetComponent<Score>();

			ScoreManager.singleton.AddScore(script);

			Finish();
		}
	}
}
