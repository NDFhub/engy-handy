using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Game")]
	public class NextItemHard : FsmStateAction
	{

		public override void OnEnter()
		{
			Main.singleton.NextItem();
			Finish();
		}


	}

}
