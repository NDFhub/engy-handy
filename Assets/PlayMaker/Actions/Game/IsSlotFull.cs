using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Game")]
	public class IsSlotFull : FsmStateAction
	{
		[RequiredField]
		[CheckForComponent(typeof(ObjectSlot))]
		public FsmGameObject gameObject;

		public PopupTextContainer fulledText;
		public PopupTextContainer notText;

		public override void OnEnter()
		{
			var script = gameObject.Value.GetComponent<ObjectSlot>();

			if (fulledText)
            {
				if (script.IsSloted)
                {
					Failed(fulledText);
					return;
                }
				else
                {
					Finish();
                }
            }
			else
            {
				if (script.IsSloted == false)
                {
					Failed(notText);
					return;
                }
				else
                {
					Finish();
                }
            }
		}

		void Failed(PopupTextContainer text)
		{
			if (text)
				ScoreManager.singleton.AddWrong(text);
			// show error
			Fsm.SetState(Fsm.PreviousActiveState.Name);

		}
	}

}
