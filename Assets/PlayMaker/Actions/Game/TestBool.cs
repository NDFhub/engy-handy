using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Game")]
	[ActionTarget(typeof(PlayMakerFSM), "gameObject,fsmName")]
	public class TestBool : FsmStateAction
	{
		[RequiredField]
		[Tooltip("The GameObject that owns the FSM.")]
		public FsmGameObject gameObject;

		[UIHint(UIHint.FsmName)]
		[Tooltip("Optional name of Fsm on Game Object. Useful if there is more than one FSM on the GameObject.")]
		public FsmString fsmName;

		[RequiredField]
		[UIHint(UIHint.FsmBool)]
		public FsmString varName;
		public bool value;

		// store game object last frame so we know when it's changed
		// and have to cache a new fsm
		private GameObject previousGo;

		// cache the fsm component since that's an expensive operation
		private PlayMakerFSM fsm;

		public PopupTextContainer failedText;

		public override void Reset()
		{
			gameObject = null;
			fsmName = null;
		}

		public override void OnEnter()
		{
			DoFsmStateTest();
		}

		void DoFsmStateTest()
		{
			var go = gameObject.Value;
			if (go == null)
			{
				Failed();
				return;
			}

			if (go != previousGo)
			{
				fsm = ActionHelpers.GetGameObjectFsm(go, fsmName.Value);
				previousGo = go;
			}

			if (fsm == null)
			{
				Failed();
				return;
			}

			var my_var = fsm.FsmVariables.GetFsmBool(varName.Value);

			if (my_var == null)
			{
				Failed();
				return;
			}

			if (my_var.Value == value)
			{
				Finish();
			}
			else
			{
				Failed();
				return;
			}
		}

		void Failed()
		{
			if (failedText)
				ScoreManager.singleton.AddWrong(failedText);
			Fsm.SetState(Fsm.PreviousActiveState.Name);
		}
	}
}
