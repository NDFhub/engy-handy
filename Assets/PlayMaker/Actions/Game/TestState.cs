using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Game")]
	[ActionTarget(typeof(PlayMakerFSM), "gameObject,fsmName")]
	[Tooltip("Tests if an FSM is in the specified State.")]
	public class TestState : FsmStateAction
	{
		[RequiredField]
		[Tooltip("The GameObject that owns the FSM.")]
		public FsmGameObject gameObject;

		[UIHint(UIHint.FsmName)]
		[Tooltip("Optional name of Fsm on Game Object. Useful if there is more than one FSM on the GameObject.")]
		public FsmString fsmName;

		[RequiredField]
		[Tooltip("Check to see if the FSM is in this state.")]
		public FsmString stateName;

		// store game object last frame so we know when it's changed
		// and have to cache a new fsm
		private GameObject previousGo;

		// cache the fsm component since that's an expensive operation
		private PlayMakerFSM fsm;

		public PopupTextContainer failedText;

		public override void Reset()
		{
			gameObject = null;
			fsmName = null;
			stateName = null;
		}

		public override void OnEnter()
		{
			DoFsmStateTest();
		}

		void DoFsmStateTest()
		{
			var go = gameObject.Value;
			if (go == null)
			{
				Failed();
				return;
			}

			if (go != previousGo)
			{
				fsm = ActionHelpers.GetGameObjectFsm(go, fsmName.Value);
				previousGo = go;
			}

			if (fsm == null)
			{
				Failed();
				return;
			}

			if (fsm.ActiveStateName == stateName.Value)
			{
				Finish();
				return;
			}
			else
			{
				Failed();
				return;
			}
		}

		void Failed()
		{
			if (failedText)
				ScoreManager.singleton.AddWrong(failedText);
			// show error
			Fsm.SetState(Fsm.PreviousActiveState.Name);
			
		}
	}

}
