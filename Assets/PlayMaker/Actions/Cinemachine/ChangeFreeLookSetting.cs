using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Cinemachine")]
	public class ChangeFreeLookSetting : FsmStateAction
	{
		[RequiredField]
		[CheckForComponent(typeof(ViewSetting))]
		public FsmOwnerDefault gameObject;
		ViewSetting script;

		public override void OnEnter()
		{
			if (script == null)
			{
				var go = Fsm.GetOwnerDefaultTarget(gameObject);
				script = go.GetComponent<ViewSetting>();
			}

			if (Main.singleton.selectGame.easy)
				script.To(2f, () =>
				{

				});

			Finish();
		}
	}
}
