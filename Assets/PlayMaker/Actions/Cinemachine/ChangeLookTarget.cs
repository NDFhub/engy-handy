using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Cinemachine")]
	public class ChangeLookTarget : FsmStateAction
	{
		public Transform target;

		public override void OnEnter()
		{
			if (target)
			{
				var control = GameObject.FindObjectOfType<PanCameraControl>();
				if (control)
					control.ChangeLookAt(target);
			}
			
			Finish();
		}
	}

}
