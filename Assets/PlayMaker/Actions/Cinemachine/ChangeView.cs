using UnityEngine;
using Cinemachine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Cinemachine")]
	public class ChangeView : FsmStateAction
	{
		[RequiredField]
		[CheckForComponent(typeof(CinemachineVirtualCamera))]
		public FsmOwnerDefault gameObject;

		int count;

		CinemachineVirtualCamera cam;
		CinemachineVirtualCamera[] all;

		public override void OnEnter()
		{
			// start 0
			count++;
			if (count > 100)
				count = 2;

			if (count == 1)
            {
				if (is_start_state) // skip start if default
				{
					Finish();
					return;
				}
			}

			if (cam == null)
			{
				var go = Fsm.GetOwnerDefaultTarget(gameObject);
				cam = go.GetComponent<CinemachineVirtualCamera>();
			}

			if (all == null)
				all = GameObject.FindObjectsOfType<CinemachineVirtualCamera>(true);

			if (cam)
			{
				foreach (var item in all)
					if (item.Priority > 0)
						item.Priority = 0;

				cam.Priority = 100;
			}

			Finish();
		}

		bool is_start_state
        {
			get
            {
				return this.State.Name == Fsm.StartState;
            }
        }

	}

}
