using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory(ActionCategory.GameObject)]
	public class SetGameObjects : FsmStateAction
	{
		[System.Serializable]
		public class Entry
        {
			public GameObject go;
			public bool show;
        }

		public Entry[] list;

		// Code that runs on entering the state.
		public override void OnEnter()
		{
			Set();
			Finish();
		}


		void Set ()
        {
            foreach (var item in list)
            {
				item.go.SetActive(item.show);
            }
        }


	}

}
