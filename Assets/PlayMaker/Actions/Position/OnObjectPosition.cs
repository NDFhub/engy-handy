using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Position")]
	public class OnObjectPosition : FsmStateAction
	{

		public override void OnEnter()
		{
			var pos = Owner.GetComponent<ObjectPosition>();
			if (pos)
			{
				pos.UpdatePosition();
			}

			Finish();
		}
	}
}
