using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Objective")]
	public class ClearObjective : FsmStateAction
	{

		public override void OnEnter()
		{
			ObjectiveManager.singleton.clear_instanced();
			Finish();
		}
	}

}
