using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Objective")]
	public class SetObjective : FsmStateAction
	{
		[RequiredField]
		[CheckForComponent(typeof(ObjectiveLine))]
		public FsmOwnerDefault gameObject;
		ObjectiveLine[] list;

		public override void OnEnter()
		{
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			list = go.GetComponentsInChildren<ObjectiveLine>();

			ObjectiveManager.singleton.RunTest += Test;
			ObjectiveManager.Set(list);
		}

        public override void OnExit()
        {
			ObjectiveManager.singleton.RunTest -= Test;
        }

		bool done;
		void Test()
		{
			done = true;
			foreach (var item in list)
			{
				if (item.Test() == false)
					done = false;
			}

			if (done)
				Finish();
		}
	}
}
