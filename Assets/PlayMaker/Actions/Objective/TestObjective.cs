using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Objective")]
	public class TestObjective : FsmStateAction
	{
		public override void OnEnter()
		{
			if (ObjectiveManager.singleton.RunTest != null)
				ObjectiveManager.singleton.RunTest.Invoke();

			Finish();
		}
	}

}
