using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleContro : MonoBehaviour
{
    // Start is called before the first frame update
    private ParticleSystem _ParticleSystem;
    void Start()
    {
        _ParticleSystem = GetComponent<ParticleSystem>();
    }

    private void OnEnable()
    {
        _ParticleSystem.Play();
    }
    
    
    // Update is called once per frame
    void Update()
    {
        
        
    }
}
