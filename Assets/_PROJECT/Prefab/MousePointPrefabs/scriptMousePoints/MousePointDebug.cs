
using UnityEngine;


public class MousePointDebug : MonoBehaviour
{
    [SerializeField] 
    private bool UseMousePoint = true;
    
   
    private Vector2 pos;
    private Canvas myCanvas;

    [SerializeField] 
    private GameObject instantiateGameObject;
    
    void Start()
    {
        
        myCanvas = GetComponentInParent<Canvas>();
        
    }
    
    void Update()
    {
        if (UseMousePoint)
        {
            mousePointEffect();
        }
    }

   
    void mousePointEffect()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
            RectTransformUtility.ScreenPointToLocalPointInRectangle(myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);
            
            GameObject _InstantiateOJ = Instantiate(instantiateGameObject,myCanvas.transform.TransformPoint(pos),Quaternion.identity);
            _InstantiateOJ.transform.SetParent(gameObject.transform);
            
        }
        
    }
    
        
}
