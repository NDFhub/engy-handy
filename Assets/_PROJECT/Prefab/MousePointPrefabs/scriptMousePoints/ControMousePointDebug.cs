
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;


public class ControMousePointDebug : MonoBehaviour
{
    //[SerializeField] private int _FrameRate = 30;
    [SerializeField] float animateTime = 0.75f;
    private Image mImage = null;

    [SerializeField] private Sprite[] _mainSprite;
    [SerializeField] private bool _Loop = false;
    private Sprite[] mSprites = null;
    private float mTimePerFrame = 0f;
    private float mElapsedTime = 0f;
    private int mCurrentFrame = 0;

    void Start()
    {
        mImage = gameObject.GetComponent<Image>();
        enabled = false;
        LoadSpriteSheet();
    }

    private void LoadSpriteSheet()
    {
        mSprites = _mainSprite;
        if (mSprites != null && mSprites.Length > 0)
        {
            //mTimePerFrame = 1f / _FrameRate;
            mTimePerFrame = animateTime / _mainSprite.Length;
            Play();
        }

    }

    private void Play()
    {
        enabled = true;
    }


    void Update()
    {
        mElapsedTime += Time.deltaTime;
        if (mElapsedTime >= mTimePerFrame)
        {
            mElapsedTime = 0f;
            ++mCurrentFrame;
            SetSprite();
            if (mCurrentFrame >= mSprites.Length)
            {
                if (_Loop)
                {
                    mCurrentFrame = 0;
                }
                else
                {
                    Destroy(gameObject);
                }
            }

        }
    }

    void SetSprite()
    {
        if (mCurrentFrame >= 0 && mCurrentFrame < mSprites.Length)
        {
            mImage.sprite = mSprites[mCurrentFrame];
        }
    }
}
