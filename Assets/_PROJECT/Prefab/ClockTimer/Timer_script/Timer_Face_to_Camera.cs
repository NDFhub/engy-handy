using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Timer_Face_to_Camera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var target = Camera.main;
        
        var n = target.transform.position - transform.position; 
       // transform.rotation = Quaternion.LookRotation( n );
       transform.rotation = Quaternion.LookRotation(n) * Quaternion.Euler(0, 180, 0);
    }
}
