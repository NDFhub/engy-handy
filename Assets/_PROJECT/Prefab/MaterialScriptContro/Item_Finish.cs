using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Finish : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject _ItemForEffectOutlineContro;
    private GameObject _ItemForEffectOutline;
    private MeshRenderer _MainGame;
    

    //[SerializeField] private Material _TranformMaterial;

    [SerializeField] private GameObject _mainObject;
    [SerializeField] private Color _outLineColor = new Color(255,255,255,255);
    [SerializeField] private float _outlineWidth = 2;
    [SerializeField] private int _waveSponNumber = 2;
    
    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;
    private Outline _outLine;
    private OutlineEffect _outLineEffectcontro;
    void Start()
    {
        
       /* _ItemForEffectOutlineContro = new GameObject("EffectOutlineContro");
        _ItemForEffectOutlineContro.transform.parent = gameObject.transform;
        _ItemForEffectOutlineContro.transform.localPosition = new Vector3(0, 0, 0);
        _ItemForEffectOutlineContro.SetActive(false);*/
        
        gameObject.SetActive(false);
        
        for (int i = 1; i != _waveSponNumber + 1; i++)
        {
            _ItemForEffectOutline = new GameObject("EffectOutline");
            _ItemForEffectOutline.transform.parent = gameObject.transform;
            _ItemForEffectOutline.transform.localPosition = new Vector3(0, 0, 0);
        
            meshRenderer = _ItemForEffectOutline.AddComponent<MeshRenderer>();
            meshFilter = _ItemForEffectOutline.AddComponent<MeshFilter>();
           // meshFilter.mesh = gameObject.GetComponent<MeshFilter>().mesh;

            meshFilter.mesh = _mainObject.GetComponent<MeshFilter>().mesh;
            
          
           /* _outLine = _ItemForEffectOutline.AddComponent<Outline>();
            _outLine.OutlineColor = _outLineColor;
            _outLine.OutlineWidth = _outlineWidth;*/
        
            _outLineEffectcontro = _ItemForEffectOutline.AddComponent<OutlineEffect>();
           
            
        }
        

        ///
    }
    
    void Update()
    {
        
    }
}
