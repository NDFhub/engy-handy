using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[DisallowMultipleComponent]
public class OutlineEffect : MonoBehaviour
{
    public static HashSet<Mesh> registeredMeshes = new HashSet<Mesh>();
    private int _numberEffect;
    private float _rageEdit ;
    
    private class ListVector3 {
        public List<Vector3> data;
    }
   
    [SerializeField] private float _Maxrage = 1.3f;
    [SerializeField] private float _speedWhenBegin = 16f;
    [SerializeField] private float _speedWhenDisapere = 6f;
    [SerializeField] private Color outlineColor = Color.white;

    [SerializeField, Range(0f, 10f)] private float outlineWidth;
     private float reset_speed ;
    //[SerializeField,Range(0f,1f)] 
    private float _ColorValue ;
    
   

   // [SerializeField] private float _WaitTime = 2;
    
    private Material outlineFillMaterial;
    private Material outlineMaskMaterial;
    
    private Color _Outline;
    private Renderer[] renderers;
   
    
    [SerializeField, HideInInspector]
    public List<Mesh> bakeKeys = new List<Mesh>();
    
    [SerializeField, HideInInspector]
    private List<ListVector3> bakeValues = new List<ListVector3>();
    
    private bool begincontdown;
    
    private void Awake()
    {
       // meshFilter.mesh = _mainObject.GetComponent<MeshFilter>().mesh;
        renderers = GetComponentsInChildren<Renderer>();

        MeshRenderer _meshRenderer = gameObject.GetComponent<MeshRenderer>();
        _meshRenderer.material = Instantiate(Resources.Load<Material>(@"MaterialsOutlineForItem/TransparentMaterial"));
        outlineMaskMaterial = Instantiate(Resources.Load<Material>(@"MaterialsOutlineForItem/Outline_Mask_Item"));
        outlineFillMaterial = Instantiate(Resources.Load<Material>(@"MaterialsOutlineForItem/Outline_Fill_Item"));

        outlineMaskMaterial.name = "OutlineMask(Instance)";
        outlineFillMaterial.name = "OutlineFill(Instance)";

        LoadSmoothNormals();
    
        outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
        reset_speed = _speedWhenBegin;
        _Outline  = outlineColor;
    }
    
    void LoadSmoothNormals() {

        // Retrieve or generate smooth normals
        foreach (var meshFilter in GetComponentsInChildren<MeshFilter>()) {
           
            // Skip if smooth normals have already been adopted
            if (!registeredMeshes.Add(meshFilter.sharedMesh)) {
                continue;
            }

            // Retrieve or generate smooth normals
            var index = bakeKeys.IndexOf(meshFilter.sharedMesh);
            var smoothNormals = (index >= 0) ? bakeValues[index].data : SmoothNormals(meshFilter.sharedMesh);

            // Store smooth normals in UV3
           meshFilter.sharedMesh.SetUVs(3, smoothNormals);

            // Combine submeshes
            var renderer = meshFilter.GetComponent<Renderer>();

            if (renderer != null) {
                CombineSubmeshes(meshFilter.sharedMesh, renderer.sharedMaterials);
            }
        }

        // Clear UV3 on skinned mesh renderers
        foreach (var skinnedMeshRenderer in GetComponentsInChildren<SkinnedMeshRenderer>()) {

            // Skip if UV3 has already been reset
            if (!registeredMeshes.Add(skinnedMeshRenderer.sharedMesh)) {
                continue;
            }

            // Clear UV3
            skinnedMeshRenderer.sharedMesh.uv4 = new Vector2[skinnedMeshRenderer.sharedMesh.vertexCount];

            // Combine submeshes
            CombineSubmeshes(skinnedMeshRenderer.sharedMesh, skinnedMeshRenderer.sharedMaterials);
        }
    }
    
    List<Vector3> SmoothNormals(Mesh mesh) {

        // Group vertices by location
        var groups = mesh.vertices.Select((vertex, index) => new KeyValuePair<Vector3, int>(vertex, index)).GroupBy(pair => pair.Key);

        // Copy normals to a new list
        var smoothNormals = new List<Vector3>(mesh.normals);

        // Average normals for grouped vertices
        foreach (var group in groups) {

            // Skip single vertices
            if (group.Count() == 1) {
                continue;
            }

            // Calculate the average normal
            var smoothNormal = Vector3.zero;

            foreach (var pair in group) {
                smoothNormal += smoothNormals[pair.Value];
            }

            smoothNormal.Normalize();

            // Assign smooth normal to each vertex
            foreach (var pair in group) {
                smoothNormals[pair.Value] = smoothNormal;
            }
        }

        return smoothNormals;
    }
   /* [SerializeField, Tooltip("Precompute enabled: Per-vertex calculations are performed in the editor and serialized with the object. "
                             + "Precompute disabled: Per-vertex calculations are performed at runtime in Awake(). This may cause a pause for large meshes.")]
    private bool precomputeOutline;
    void OnValidate() {

        // Update material properties
      

        // Clear cache when baking is disabled or corrupted
        if (!precomputeOutline && bakeKeys.Count != 0 || bakeKeys.Count != bakeValues.Count) {
            bakeKeys.Clear();
            bakeValues.Clear();
        }

        // Generate smooth normals when baking is enabled
        if (precomputeOutline && bakeKeys.Count == 0) {
            Bake();
        }
    }
    /*void Bake() {

        // Generate smooth normals for each mesh
        var bakedMeshes = new HashSet<Mesh>();

        foreach (var meshFilter in GetComponentsInChildren<MeshFilter>()) {

            // Skip duplicates
            if (!bakedMeshes.Add(meshFilter.sharedMesh)) {
                continue;
            }

            // Serialize smooth normals
            var smoothNormals = SmoothNormals(meshFilter.sharedMesh);

            bakeKeys.Add(meshFilter.sharedMesh);
            bakeValues.Add(new ListVector3() { data = smoothNormals });
        }
    }*/
    void CombineSubmeshes(Mesh mesh, Material[] materials) {

        // Skip meshes with a single submesh
        if (mesh.subMeshCount == 1) {
            return;
        }

        // Skip if submesh count exceeds material count
        if (mesh.subMeshCount > materials.Length) {
            return;
        }

        // Append combined submesh
        mesh.subMeshCount++;
        mesh.SetTriangles(mesh.triangles, mesh.subMeshCount - 1);
    }
    private void OnEnable()
    {
        
        settime = false;
        _rageEdit = 0;
        _ColorValue = 1;
        _Outline = outlineColor;
        outlineFillMaterial.SetColor("_OutlineColor", _Outline);
        ///////
        
       
        foreach (var renderer in renderers) {

            // Append outline shaders
            var materials = renderer.sharedMaterials.ToList();

            materials.Add(outlineMaskMaterial);
            materials.Add(outlineFillMaterial);

            renderer.materials = materials.ToArray();
        }
        
        
        
        
    }
    private void OnDisable()
    {
        begincontdown = false;
       
        _speedWhenBegin = reset_speed;
        gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
        
        _Outline = _sameColor ;
        
        
        foreach (var renderer in renderers) {

            // Remove outline shaders
            var materials = renderer.sharedMaterials.ToList();

            materials.Remove(outlineMaskMaterial);
            materials.Remove(outlineFillMaterial);

            renderer.materials = materials.ToArray();
        }
        
        
    }
    
    void OnDestroy() 
    {
        // Destroy material instances
        Destroy(outlineMaskMaterial);
        Destroy(outlineFillMaterial);
    }

    
    private Color _sameColor;
    private void _Color()
    {
        if (_ColorValue > 0)
        {
            _ColorValue -= _speedWhenDisapere * 0.01f;
            _Outline = new Color(outlineColor.r,outlineColor.g,outlineColor.b, _ColorValue);
            outlineFillMaterial.SetColor("_OutlineColor", _Outline);
        }
        else
        {
            _ColorValue = 0;
            _Outline = new Color(outlineColor.r,outlineColor.g,outlineColor.b, _ColorValue);
            outlineFillMaterial.SetColor("_OutlineColor", _Outline);
            
            gameObject.SetActive(false);
           //Destroy(gameObject);
        }
    }

    private bool settime;
    private void updaterage()
    {
        if(_rageEdit < 1)
        {
            _rageEdit += _speedWhenBegin * 0.01f ;
            rageofEffect = Vector3.Lerp(new Vector3(1f, 1f, 1f), new Vector3(_Maxrage, _Maxrage, _Maxrage), _rageEdit);
            gameObject.transform.localScale = rageofEffect;
        }
        else
        {
            rageofEffect =   rageofEffect = new Vector3(_Maxrage, _Maxrage, _Maxrage);
            _Color();
        }
        
        
        
    }
    private float timetowait = 0;
    
   
    private Vector3 rageofEffect;
    private void LateUpdate()
    {
    
        updaterage();
       
    }
   
   
 
    
    
}
