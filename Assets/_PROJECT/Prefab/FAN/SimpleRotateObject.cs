using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotateObject : MonoBehaviour
   
{
    [SerializeField] public float RotationSpeed;
    [SerializeField] public GameObject gameObject;
    [SerializeField] public GameObject IsActive;


    void Start()
    {
        
    }

   
    void Update()
    {
        //FAN ON OPEN
        if ( IsActive.activeSelf == true) 
        {
            gameObject.transform.Rotate(0, 0, RotationSpeed * Time.deltaTime);
            RotationSpeed += 360 * Time.deltaTime;
            if (RotationSpeed >= 1080)
            {
                RotationSpeed = 1080;
            }
        }


        // FAN ON CLOSE
        if(IsActive.activeSelf == false)
        {
            gameObject.transform.Rotate(0, 0, RotationSpeed * Time.deltaTime);
            RotationSpeed -= 360 * Time.deltaTime;
            if(RotationSpeed <= 0)
            {
                RotationSpeed = 0;
                gameObject.transform.Rotate(0, 0, 0);
            }
        }
    }
}
