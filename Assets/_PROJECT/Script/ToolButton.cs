using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(ToolObject))]
public class ToolButton : MonoBehaviour
{
    MyTool main;
    ToolObject tool;
    AudioSource audioSource;
    public AudioClip click;
    // [SerializeField] TextMeshProUGUI text;
    // [SerializeField] Image icon;
    public GameObject selected;
    public Image icon;


    public bool IsSelected
    {
        get
        {
            return selected.activeSelf;
        }
    }

    private void Start()
    {
        // if (text == null)
        //     text = GetComponentInChildren<TextMeshProUGUI>();
        // if (icon == null)
        //     icon = GetComponentInChildren<Image>();

        tool = GetComponent<ToolObject>();
        GetComponent<Button>().onClick.AddListener(OnClick);
        audioSource = GetComponent<AudioSource>();
        // text.SetText(tool.label);
        // icon.sprite = tool.icon;
    }

    void OnClick()
    {
        if (main == null)
            main = GameObject.FindObjectOfType<MyTool>();

        main.Select(tool);
    }

    public void OnSelected(bool select)
    {
        if (select)
        {
            transform.localScale = Vector3.one * 1.25f;
            selected.gameObject.SetActive(true);

            if (AudioListener.pause == false)
                audioSource.PlayOneShot(click);
        }
        else
        {
            transform.localScale = Vector3.one;
            selected.gameObject.SetActive(false);
        }

    }
}
