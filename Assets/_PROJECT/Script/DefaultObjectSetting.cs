using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefaultObjectSetting : MonoBehaviour
{
    public ViewSetting view;
    public Transform panView;
    public Transform lookAt;
    public float fov = 40;
    public float min = 15;
    public float max = 50;

    void OnEnable()
    {
        if (view)
            view.To(0.25f, () => { });

        var control = GameObject.FindObjectOfType<PanCameraControl>();
        if (control)
        {
            if (panView)
                control.ChangeView(panView);
            if (lookAt)
            {
                control.ChangeLookAt(lookAt);
            }

            control.control.target_fov = fov;
        }

        var zoom_ui = UIComponent.Get("ZOOM");
        if (zoom_ui)
        {
            var zoom = zoom_ui.GetComponent<Slider>();
            zoom.maxValue = max;
            zoom.minValue = min;
            zoom.value = fov;
        }
    }
}
