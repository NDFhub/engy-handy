using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ToolEventFsm : MonoBehaviour
{
    PlayMakerFSM fsm;
    MyTool tool;

    void Start()
    {
        fsm = GetComponent<PlayMakerFSM>();
        if (tool == null)
            tool = GameObject.FindObjectOfType<MyTool>();

        if (tool)
        {
            tool.event_fsm = this;
        }
    }

    public void SelectTool(string tool)
    {
        var find = fsm.Fsm.ActiveState.Transitions.FirstOrDefault(x => x.FsmEvent.Name == tool);
        if (find != null)
        {
            StartCoroutine(SendEventThenSetState(tool));
        }
        else
        {
            var state = fsm.FsmStates.FirstOrDefault(x => x.Name == tool);
            if (state != null)
                fsm.SetState(tool);
            else
                fsm.SendEvent("START");//default state
        }
    }

    IEnumerator SendEventThenSetState(string tool)
    {
        fsm.SendEvent(tool);
        yield return null;
        fsm.SetState(tool);
    }

}
