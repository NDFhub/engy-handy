using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

public class ObjectiveLine : MonoBehaviour
{
    public string text;

    public bool sub;


    [ShowIf("sub")]
    [HorizontalGroup("Group 1")]
    public string group;

    [ShowIf("@this.InGroup && this.sub")]
    [HorizontalGroup("Group 1", Width = 0)]
    [HideLabel]
    public bool mainGroup;


    [ShowIf("@this.sub")]
    public PlayMakerFSM fsm;

    [ShowIf("@this.sub && this.fsm != null")]
    public bool checkState;
    [ShowIf("@this.sub && this.checkState")]
    [ValueDropdown("get_state")]
    public string stateName;

    [ShowIf("@this.sub && this.fsm != null")]
    public bool checkVariable;
    [ShowIf("@this.sub && this.checkVariable")]
    [ValueDropdown("get_var")]
    [HorizontalGroup("Group 2")]
    public string variableName;
    [ShowIf("@this.sub && this.checkVariable")]
    [HorizontalGroup("Group 2")]
    [HideLabel]
    public bool isTrue;

    public ObjectiveText bindUI { set; get; }

    private Outline _outline;

    public Outline outline
    {
        get
        {

            if (_outline == null)
            {
                _outline = fsm.GetComponent<Outline>();
            }
            return _outline;
        }
    }


    public bool InGroup
    {
        get
        {
            return string.IsNullOrEmpty(group) == false;
        }
    }

    public bool Test()
    {
        if (sub == false)
        {
            SetBindUI(true);
            return true;
        }

        var s1 = CheckState();
        var s2 = CheckBool();

        if (s1 && s2)
        {
            SetBindUI(true);
            return true;
        }

        SetBindUI(false);
        return false;
    }

    void SetBindUI(bool done)
    {
        // var group
        if (mainGroup)
        {
            var count = 0;
            var max = 0;
            var list = ObjectiveManager.singleton.instanced.Where(x => x.data.group == group);
            max = list.Count() - 1;
            foreach (var item in list)
            {
                if (item.data != this)
                    if (item.data.Test())
                        count++;
            }

            bindUI.SetText(text + "(" + count + "/" + max + ")");
            ObjectiveManager.Rebuild();
            bindUI.SetDone(count == max);
        }
        else
        {
            if (InGroup)
                return;

            if (bindUI)
                bindUI.SetDone(done);
        }
    }

    bool CheckState()
    {
        if (checkState)
        {
            if (fsm)
            {
                if (fsm.ActiveStateName == stateName)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        return true;
    }

    bool CheckBool()
    {
        if (checkVariable)
        {
            if (fsm)
            {
                if (fsm.FsmVariables.GetFsmBool(variableName).Value == isTrue)
                    return true;
                else
                    return false;
            }
        }

        return true;
    }


    IEnumerable get_state()
    {
        if (fsm == null)
            return "";

        return fsm.FsmStates.Select(x => x.Name);
    }

    IEnumerable get_var()
    {
        if (fsm == null)
            return "";

        //if (fsm.FsmVariables.BoolVariables.Length <= 0)
        //    return "";

        return fsm.FsmVariables.BoolVariables.Select(x => x.Name);
    }
}
