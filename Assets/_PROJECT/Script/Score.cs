using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public string id;
    public int order;
    public string label;
    public int score;

    public override string ToString()
    {
        return label + ": " + score;
    }
}
