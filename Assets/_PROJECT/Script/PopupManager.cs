using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class PopupManager : MonoBehaviour
{
    public bool Queue = false;
    public Transform root;
    public GameObject prefab;

    public List<Entry> list;

    int count;
    string prev;

    void Start()
    {
        
    }

    bool showErrorOnHardMode (string text)
    {
        var hard_mode = Main.singleton.selectGame.easy == false;
        if (hard_mode)
        {
            if (prev == text)
            {
                count++;
            }
            else
            {
                count = 0;
            }
            prev = text;

            if (count >= 4)
            {
                count = -1;
                return true; //show error
            }
            else
            {
                return false; // skip
            }
        }

        return true; // show easy mode
    }



    public void AddError(string text)
    {
        if (showErrorOnHardMode(text) == false)
            return;

        if (Queue == false)
        {
            // no queue
            if (list.Count > 0)
                return;
        }

        var entry = new Entry()
        {
            txt = text,
        };

        list.Add(entry);

        if (list.Count == 1)
            StartCoroutine(show());
    }

    WaitForSeconds wait3 = new WaitForSeconds(3);
    WaitForSeconds wait1 = new WaitForSeconds(1);
    Vector3 start_scale = new Vector3(0, 1, 1);
    IEnumerator show()
    {
        var go = GameObject.Instantiate<GameObject>(prefab);
        go.transform.SetParent(root, false);
        go.transform.localScale = start_scale;
        var txt = go.GetComponentInChildren<TextMeshProUGUI>();
        txt.SetText(list[0].txt);

        go.SetActive(true);
        go.transform.DOScaleX(1f, 0.25f).SetEase(Ease.OutBack);

        yield return wait3;
        GameObject.Destroy(go);

        yield return wait1;

        list.RemoveAt(0);
        if (list.Count > 0)
            StartCoroutine(show());
    }

    [System.Serializable]
    public class Entry
    {
        public string txt;
    }
}
