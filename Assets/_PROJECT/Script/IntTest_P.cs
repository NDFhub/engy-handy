using System;
using UnityEngine;
using UnityEngine.Networking;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory(ActionCategory.Logic)]
    [Tooltip("Sends Events based on the value of a Boolean Variable.")]

    public class IntTest_P : FsmStateAction
    {
        [RequiredField]
        [UIHint(UIHint.Variable),Readonly]
        [Tooltip("The Bool variable to test.")]
        public FsmInt intVariable;

        [Tooltip("Event to send if the Bool variable is True.")]
        public FsmInt finalvalue;
        

        [Tooltip("Repeat every frame while the state is active.")]
        public bool everyFrame;

        public override void Reset()
        {
            intVariable = null;
            finalvalue = null;
            everyFrame = false;
        }

        public override void OnEnter()
        {
            /*Debug.Log("OnEnter");
            Debug.Log(intVariable);
            Debug.Log(finalvalue);*/
           
            if (finalvalue.Value == intVariable.Value)
            {
                Debug.Log("Finish");
                Finish();
            }
            
           /* Debug.Log(intVariable);
            Debug.Log(finalvalue);*/
            
            if (!everyFrame)
            {
                Finish();
            }
        }
		
        public override void OnUpdate()
        {
            
            if (intVariable.Value == finalvalue.Value)
            {
                Finish();
            }
         
        }
    }
}