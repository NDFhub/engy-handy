using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PositionTool : MonoBehaviour
{

    [FoldoutGroup("Tool")]
    [Button("Clone")]
    void clone ()
    {
        // clone to other

        var go = GameObject.Instantiate<GameObject>(gameObject);
    }
    [FoldoutGroup("Tool")]
    [Button("Remove Components")]
    void remove()
    {
        // remove all components
        var components = GetComponentsInChildren<Component>();
        foreach (var item in components)
        {
            //Debug.Log(item);

            if (item is Transform)
                continue;

            if (item is PositionTool)
                continue;

            if (item is ObjectPosition)
                continue;

            GameObject.DestroyImmediate(item);
        }

        // remove mesh filter mesh renderer
        // remove 
    }
}
