using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MyTool : MonoBehaviour
{
    public static MyTool singleton;
    public ToolObject current;
    public ToolEventFsm event_fsm;

    public GameObject prefab;
    public Transform dataRoot;
    public Transform root;

    // Start is called before the first frame update
    void Start()
    {
        singleton = this;
        init();
    }

    void init()
    {
        var scripts = dataRoot.GetComponentsInChildren<ToolData>();
        foreach (var item in scripts)
        {
            var go = GameObject.Instantiate<GameObject>(prefab, root);
            // set data
            var tool_object = go.GetComponent<ToolObject>();
            tool_object.id = item.id;
            var tool_button = go.GetComponent<ToolButton>();
            tool_button.icon.sprite = item.icon;
            tool_button.click = item.click;

            go.name = "TOOL " + item.id;
            go.SetActive(true);
        }
    }

    public const string VIEW = "VIEW";
    public const string CLOTH = "CLOTH";
    public const string WET_CLOTH = "WET_CLOTH";
    public const string BRUSH = "BRUSH";
    public const string WATER = "WATER";
    public const string VINEGAR = "VINEGAR";
    public const string LADLE = "LADLE";
    public const string SPONGE = "SPONGE";
    
    public static string CurrentToolID
    {
        get
        {
            if (singleton.current == null)
                return VIEW;
            return singleton.current.id;
        }
    }

    public void Select(ToolObject selected)
    {
        if (current != null && current.id == selected.id)
        {
            current.button.OnSelected(false);
            current = null; // select same one = deselect
            if (event_fsm)
            {
                event_fsm.SelectTool("NONE");
            }
        }
        else
        {
            // resize old one
            if (current)
                current.button.OnSelected(false);

            current = selected;
            current.button.OnSelected(true);

            if (event_fsm)
            {
                event_fsm.SelectTool(current.id);
            }
        }
    }

    public void Used(ClickWithTool with)
    {
        // used event
        if (event_fsm)
        {
            event_fsm.SelectTool("USE_" + current.id);
        }
    }

    public void Deselect()
    {
        if (current)
            current.button.OnSelected(false);
        current = null;
    }

    public void EnableTool(string id)
    {
        var tools = GameObject.FindObjectsOfType<ToolObject>(true);
        var first = tools.FirstOrDefault(x => x.id == id);
        if (first)
        {
            first.gameObject.SetActive(true);
        }
    }
    public void DisableTool(string id)
    {
        var tools = GameObject.FindObjectsOfType<ToolObject>(true);
        var first = tools.FirstOrDefault(x => x.id == id);
        if (first)
        {
            first.gameObject.SetActive(false);
        }
    }
}
