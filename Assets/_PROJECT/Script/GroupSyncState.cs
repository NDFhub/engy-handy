using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GroupSyncState : MonoBehaviour
{
    public string group;

    PlayMakerFSM fsm;
    IEnumerable<GroupSyncState> groups;


    void Start()
    {

    }

    public void Sync(string state)
    {
        if (groups == null)
        {
            var all = GameObject.FindObjectsOfType<GroupSyncState>();
            groups = all.Where(x => x.group == group);
        }

        foreach (var item in groups)
        {
            if (item == this)
                continue; // skip self

            item.SetState(state);
        }
    }

    void SetState(string state)
    {
        if (fsm == null)
            fsm = GetComponent<PlayMakerFSM>();

        if (fsm.ActiveStateName != state)
            fsm.SetState(state);
    }

    public void SyncBool(string var_name, bool value)
    {
        if (groups == null)
        {
            var all = GameObject.FindObjectsOfType<GroupSyncState>();
            groups = all.Where(x => x.group == group);
        }

        foreach (var item in groups)
        {
            if (item == this)
                continue; // skip self

            item.SetBool(var_name, value);
        }
    }

    void SetBool(string var_name, bool value)
    {
        if (fsm == null)
            fsm = GetComponent<PlayMakerFSM>();

        var fsm_bool = fsm.FsmVariables.FindFsmBool(var_name);
        if (fsm_bool != null)
        {
            fsm_bool.Value = value;
        }
    }

}
