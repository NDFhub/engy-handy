using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class HighlightScript : MonoBehaviour
{
    public Button button;
    // Start is called before the first frame update
    void Start()
    {

    }
    public void click()
    {
        //Shearching for subitem
        StartCoroutine(highlight());
    }

    IEnumerator highlight()
    {
        button.interactable = false;
        var list = Main.singleton.objective.instanced.Where(x => x.data.sub && x.toggle.isOn == false).Select(x => x.data.outline).ToList();

        foreach (var item in list)
        {
            item.OutlineWidth = 2;
        }

        yield return new WaitForSeconds(2);
       
        foreach (var item in list)
        {
            item.OutlineWidth = 0;
         
        }
        button.interactable = true;
    }
}
