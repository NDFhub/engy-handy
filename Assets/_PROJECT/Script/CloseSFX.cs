using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CloseSFX : MonoBehaviour
{
    private AudioSource audiosource;

    private void Start()
    {
        audiosource = GetComponent<AudioSource>();
    }
    private void Update()
    {
        if(AudioListener.pause == true)
        {
            audiosource.enabled = false;
        }
        if (AudioListener.pause == false)
        {
            audiosource.enabled = true;
        }
    }
}
