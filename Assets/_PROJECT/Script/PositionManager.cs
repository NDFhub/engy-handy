using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PositionManager : MonoBehaviour
{
    public static PositionManager singleton;
    public ObjectPosition[] list;

    void Awake()
    {
        singleton = this;
        Init();
    }

    public void Init ()
    {
        list = GameObject.FindObjectsOfType<ObjectPosition>();
    }
}
