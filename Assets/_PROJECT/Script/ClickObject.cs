using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

public class ClickObject : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    List<ClickWithTool> list;
    public bool SetFOV;
    public float fov;
    public void Click()
    {
        Debug.Log("click " + name);
        if (MyTool.CurrentToolID == MyTool.VIEW)
        {
            // select this view
            Main.singleton.freelook.SetTarget(transform);
            // set fov

            var pan = GameObject.FindObjectOfType<PanCameraControl>();
            if (pan)
            {
                if (SetFOV)
                {
                    pan.target_fov = fov;
                    UpdateSliderFOV(pan.target_fov);
                }
                else
                {
                    var default_setting = GameObject.FindObjectOfType<DefaultObjectSetting>();
                    if (default_setting)
                    {
                        pan.target_fov = default_setting.fov;
                        UpdateSliderFOV(pan.target_fov);
                    }
                }
            }

            return;
        }

        if (list == null)
            list = GetFromChildrenFirstLevel<ClickWithTool>(gameObject);

        foreach (var item in list)
        {
            if (item.IsActive())
            {
                item.OnClick();
            }
        }

        // run test
        if (coroutine != null)
            StopCoroutine(coroutine);

        coroutine = StartCoroutine(delay_test());
    }

    void UpdateSliderFOV (float fov)
    {
        var zoom_ui = UIComponent.Get("ZOOM");
        if (zoom_ui)
        {
            var zoom = zoom_ui.GetComponent<Slider>();
            zoom.value = fov;
        }
    }

    public static List<T> GetFromChildrenFirstLevel<T>(GameObject go)
    {
        var transform = go.transform;
        var temp = new List<T>();
        foreach (Transform item in transform)
        {
            var script = item.GetComponent<T>();
            if (script != null)
                temp.Add(script);
        }

        return temp;
    }

    Coroutine coroutine;
    WaitForSeconds wait = new WaitForSeconds(0.1f);
    IEnumerator delay_test()
    {
        yield return wait;
        if (ObjectiveManager.singleton.RunTest != null)
            ObjectiveManager.singleton.RunTest.Invoke();
    }

    Vector2 click_start;
    float click_time;

    public void OnPointerDown(PointerEventData eventData)
    {
        click_start = eventData.position;
        click_time = Time.time;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        var delta = (eventData.position - click_start).magnitude;
        var delta_time = Time.time - click_time;

        //Debug.Log("delta: " + delta);
        
        if (delta < 10 && delta_time < 0.5f)
        {
            Click();
            //// show click fx
            //MousePointDebug.singleton.CreateFX();
        }
    }
}
