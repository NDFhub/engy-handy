using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;
using Sirenix.OdinInspector;

public class ViewSetting : MonoBehaviour
{
    public bool changeX;
    [ShowIf("changeX")]
    public float x;

    public bool changeY;
    [ShowIf("changeY")]
    public float y;

    public bool changeFOV;
    [ShowIf("changeFOV")]
    public float fov;
    public Transform center;
    FreeLookControl control;

    public void To(float time, System.Action done)
    {
        if (control == null)
            control = GameObject.FindObjectOfType<FreeLookControl>();

        var value = 0f;
        DOTween.KillAll();
        DOTween.To(() => value, t =>
        {
            if (changeX)
                control.cam.m_XAxis.Value = Mathf.Lerp(control.cam.m_XAxis.Value, x, t);
            if (changeY)
                control.cam.m_YAxis.Value = Mathf.Lerp(control.cam.m_YAxis.Value, y, t);
            //control.cam.m_Lens.FieldOfView = Mathf.Lerp(control.cam.m_Lens.FieldOfView, fov, t);
            if (changeFOV)
                control.target_fov = Mathf.Lerp(control.target_fov, fov, t);
            if (center)
                control.target.position = Vector3.Lerp(control.target.position, center.position, t);
        }, 1f, time).SetEase(Ease.InOutSine).OnComplete(() =>
        {
            if (done != null)
                done.Invoke();
        });
    }

    [Button("Copy Value From Camera")]
    public void Copy()
    {
        var view = GameObject.FindObjectOfType<CinemachineFreeLook>();
        x = view.m_XAxis.Value;
        y = view.m_YAxis.Value;
        fov = view.m_Lens.FieldOfView;

#if UNITY_EDITOR
        UnityEditor.Undo.RecordObject(this, "Copy Camera Value");
#endif
    }

}
