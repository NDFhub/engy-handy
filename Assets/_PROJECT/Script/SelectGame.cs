using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class SelectGame : MonoBehaviour
{
    public AssetReferenceGameObject game;

    public List<AssetReferenceGameObject> hards;

    SelectGameUI ui
    {
        get
        {
            if (_ui == null)
                _ui = GameObject.FindObjectOfType<SelectGameUI>();
            return _ui;
        }
    }
    SelectGameUI _ui;


    void Start()
    {

    }

    public void ClickEasy()
    {
        UIComponent.Get("LOADING").gameObject.SetActive(true);
        game.InstantiateAsync().Completed += x =>
        {
            Main.singleton.StartEasyGame(x.Result);
            UIComponent.Get("LOADING").gameObject.SetActive(false);
            //ui.CreateNewGame(x.Result, true);
        };
    }

    public void ClickHard()
    {
        Main.singleton.StartHardGame(hards);
        //game.InstantiateAsync().Completed += x =>
        //{
        //    ui.CreateNewGame(x.Result, false);
        //};
    }


}
