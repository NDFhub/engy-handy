using Cinemachine;
using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

public class FreeLookControl : MonoBehaviour
{

    public float TouchSensitivity_x = 10f;
    public float TouchSensitivity_y = 10f;
    const string MOUSEX = "Mouse X";
    const string MOUSEY = "Mouse Y";
    public float MouseSensitivity = 5f;
    public float ScrollWheelSensitivity = 500f;

    public CinemachineFreeLook cam;

    public float target_fov;
    public Transform target;

    public List<PauseFreeLook> pauseList { get; set; }

    public bool IsPause
    {
        get
        {
            if (pauseList == null)
                pauseList = new List<PauseFreeLook>();
            return pauseList.Count > 0;
        }
    }



    void Start()
    {
        CinemachineCore.GetInputAxis = HandleAxisInputDelegate;
        target_fov = cam.m_Lens.FieldOfView;
    }

    void Update()
    {
        // add mouse scroll to zoom
        if (IsPause == false)
        {
            var scroll = Input.mouseScrollDelta;
            target_fov += -scroll.y * Time.deltaTime * ScrollWheelSensitivity;
            target_fov = Mathf.Clamp(target_fov, 6, 50);
        }
       
        cam.m_Lens.FieldOfView = Mathf.Lerp(cam.m_Lens.FieldOfView, target_fov, 8 * Time.deltaTime);
    }

    float HandleAxisInputDelegate(string axisName)
    {
        if (IsPause)
            return 0;

        if (MyTool.CurrentToolID != MyTool.VIEW)
            return 0;

        switch (axisName)
        {
            case MOUSEX:
                if (Input.touchCount > 0)
                {
                    return Input.touches[0].deltaPosition.x / TouchSensitivity_x;
                }
                else
                {
                    if (Input.GetMouseButton(0))
                        return Input.GetAxis(axisName) * MouseSensitivity;
                    else
                        return 0;
                }

            case MOUSEY:
                if (Input.touchCount > 0)
                {
                    return Input.touches[0].deltaPosition.y / TouchSensitivity_y;
                }
                else
                {
                    if (Input.GetMouseButton(0))
                        return Input.GetAxis(axisName) * MouseSensitivity;
                    else
                        return 0;
                }

            default:
                Debug.LogError("Input <" + axisName + "> not recognyzed.", this);
                break;
        }

        return 0f;
    }

    /// <summary>
    /// set lookatarget
    /// set body
    /// </summary>
    /// <param name="t"></param>
    public void SetTarget(Transform t)
    {
        DOTween.KillAll();
        target.DOMove(t.position, 0.5f);

        var pan = GameObject.FindObjectOfType<PanCameraControl>();
        pan.ChangeLookAt(t);
    }
}