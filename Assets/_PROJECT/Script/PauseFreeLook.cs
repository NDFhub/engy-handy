using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseFreeLook : MonoBehaviour
{
    FreeLookControl script
    {
        get
        {
            if (_script == null)
                _script = GameObject.FindObjectOfType<FreeLookControl>();
            return _script;
        }
    }
    FreeLookControl _script;

    private void OnEnable()
    {
        clean();

        if (script.pauseList.Contains(this) == false)
            script.pauseList.Add(this);
    }

    private void OnDisable()
    {
        if (script.pauseList.Contains(this))
            script.pauseList.Remove(this);

        clean();
    }

    void clean()
    {
        if (script.pauseList == null)
            script.pauseList = new List<PauseFreeLook>();

        for (int i = 0; i < script.pauseList.Count; i++)
        {
            if (script.pauseList[i] == null)
                script.pauseList.RemoveAt(i--);
        }
    }
}
