using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectSlot : MonoBehaviour
{
    //public List<GameObject> list;
    public GameObject go;
    //public int max = 1;
    public bool autoDefaultItemByDistance;

    public bool IsSloted
    {
        get
        {
            //return list.Count > 0;
            return go != null;
        }
    }

    IEnumerator Start()
    {
        yield return null;
        if (autoDefaultItemByDistance)
        {
            var close_item = get_default_by_distance();
            if (close_item)
            {
                //list.Add(close_item.gameObject);
                go = close_item.gameObject;
            }
        }
    }

    ClickObject[] objs;
    ClickObject get_default_by_distance()
    {
        if (objs == null)
            objs = GameObject.FindObjectsOfType<ClickObject>();
        var nears = objs.Where(x => (x.transform.position - transform.position).sqrMagnitude < 0.1f);
        var clostest = nears.OrderBy(x => (x.transform.position - transform.position).sqrMagnitude);
        return clostest.FirstOrDefault();
    }

    public void UpdateSlot()
    {
        var slot = get_default_by_distance();
        if (slot)
            go = slot.gameObject;
        else
            go = null;
    }

    //public void Add(GameObject go)
    //{
    //    if (list == null)
    //        list = new List<GameObject>();

    //    clean();

    //    if (list.Count >= max)
    //    {
    //        Debug.Log(name + "is full");
    //        return;
    //    }

    //    if (list.Contains(go) == false)
    //        list.Add(go);
    //}

    //public void Remove(GameObject go)
    //{
    //    if (list == null)
    //        list = new List<GameObject>();

    //    if (list.Contains(go))
    //        list.Remove(go);

    //    clean();
    //}

    //void clean()
    //{
    //    for (int i = 0; i < list.Count; i++)
    //    {
    //        if (list[i] == null)
    //            list.RemoveAt(i--);
    //    }
    //}
}
