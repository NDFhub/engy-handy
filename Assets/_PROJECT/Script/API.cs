using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Sirenix.OdinInspector;
using System.Text;
using TMPro;
using System.Linq;
using UnityEngine.UI;

public class API : MonoBehaviour
{
    public static API singleton;
    public int gameId = 3;
    public string url;
    public string api = "/api/v1/";
    public string token;

    public string getResources;
    public string getShop;
    public string postPlayGame;

    public int score;
    public int energy;
    public int unitPrice;
    public int playGameEnergy = 35;
    public int thomasAura = -10;
    public bool hasThomasAura;
    public bool hasMariaAura;
    public ShopItem[] shops;

    public GetResources res;
    public MeJSON me;
    //public LeaderboardJSON leaderboard;
    public LeaderboardItem[] leaderboards;


    void Awake()
    {
        singleton = this;
    }

    bool is_inited;
    public bool IsInited { get { return is_inited; } }
    IEnumerator Start()
    {
        is_inited = false;
        yield return null;
        UIComponent.Get("LOADING").gameObject.SetActive(true);

        var query_api = "";
        var get_url = URLParameters.GetSearchParameters().TryGetValue("api", out query_api);
        if (get_url && string.IsNullOrEmpty(query_api) == false)
        {
            url = query_api;
            Debug.Log("get url: " + url);
        }

        var query_token = "";
        var get = URLParameters.GetSearchParameters().TryGetValue("token", out query_token);
        if (get && string.IsNullOrEmpty(query_token) == false)
        {
            token = query_token;
            Debug.Log("get token: " + token);
        }

        yield return StartCoroutine(get_me());
        yield return StartCoroutine(get_resources());
        yield return StartCoroutine(get_shop());
        //yield return StartCoroutine(get_leaderboard());

        UIComponent.Get("LOADING").gameObject.SetActive(false);
        is_inited = true;
    }

    private void Update()
    {
        if (is_inited == false)
            return;

        if (error_delay > 0)
        {
            error_delay -= Time.deltaTime;
            return;
        }

        res.energy.timeLeft -= Time.deltaTime;
        if (res.energy.timeLeft < -1)
        {
            if (calling_api == false)
            {
                StartCoroutine(get_resources()); // call api
            }
        }
    }

    void UpdateGetResourceGraphics(GetResources r)
    {
        res = r;
        // show energy
        var energy_label = UIComponent.Get("ENERGY").GetComponentInChildren<TextMeshProUGUI>();
        energy_label.SetText(r.energy.currentEnergy.ToString());
        // show score
        var score = GetItemAmount(r.items, "score_handy");
        var score_label = UIComponent.Get("POINT").GetComponentInChildren<TextMeshProUGUI>();
        score_label.SetText(score.ToString());
        // thomas aura
        var thomas = GetItemAmount(r.items, "thomas_aura_handy");
        hasThomasAura = thomas > 0;
        var maria = GetItemAmount(r.items, "maria_aura_handy");
        hasMariaAura = maria > 0;

        this.score = score;
        this.energy = r.energy.currentEnergy;

        var need_en_label2 = UIComponent.Get("NEEDENERGY2").GetComponentInChildren<TextMeshProUGUI>();
        var need_en = NeedEnergyToPlay();
        need_en_label2.SetText(need_en.ToString());

        var button_challenge = UIComponent.Get("STARTCHALLENGE").GetComponent<Button>();
        button_challenge.interactable = this.energy >= need_en;
    }

    public int NeedEnergyToPlay()
    {
        var en = playGameEnergy;
        if (hasThomasAura)
            en += thomasAura;
        return en;
    }

    float error_delay;
    bool calling_api;
    IEnumerator get_resources()
    {
        calling_api = true;
        UnityWebRequest www = UnityWebRequest.Get(url + api + "games/resource");
        AddBearer(www);

        yield return www.SendWebRequest();

        var error = string.IsNullOrEmpty(www.error) == false;
        if (error)
        {
            error_delay = 60;
            Debug.Log("error: " + www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            var r = JsonUtility.FromJson<GetResources>(www.downloadHandler.text);
            UpdateGetResourceGraphics(r);
        }

        calling_api = false;
    }

    IEnumerator get_shop()
    {
        UnityWebRequest www = UnityWebRequest.Get(url + api + "games/shop");
        AddBearer(www);

        yield return www.SendWebRequest();

        var error = string.IsNullOrEmpty(www.error) == false;
        if (error)
        {
            Debug.Log("error: " + www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            try
            {
                shops = JsonHelper.ParseJsonArray<ShopItem[]>(www.downloadHandler.text);
            }
            catch (System.Exception)
            {

            }

            unitPrice = GetShopPrice(shops, "screwdriver_handy");
        }
    }


    public void Buy(string itemId, int amount)
    {
        if (is_buying)
            return;

        StartCoroutine(buy_item(itemId, amount));
    }

    bool is_buying;
    IEnumerator buy_item(string itemId, int amount)
    {
        is_buying = true;
        // show loading
        UIComponent.Get("LOADING").gameObject.SetActive(true);

        UnityWebRequest www = UnityWebRequest.Post(url + api + "games/buy?itemId=" + itemId + "&amount=" + amount, "");
        AddBearer(www);

        yield return www.SendWebRequest();

        Debug.Log(www.downloadHandler.text);

        if (www.responseCode > 200)
        {
            // error
        }
        else
        {
            var r = JsonUtility.FromJson<GetResources>(www.downloadHandler.text);
            UpdateGetResourceGraphics(r);
        }

        // hide loading
        UIComponent.Get("LOADING").gameObject.SetActive(false);
        is_buying = false;
    }

    public int GetItemAmount(InvItem[] items, string itemId)
    {
        if (items == null)
            return 0;

        if (items.Length <= 0)
            return 0;

        var find = items.FirstOrDefault(x => x.itemId == itemId);
        if (find == null)
            return 0;

        return find.amount;
    }
    public int GetShopPrice(ShopItem[] shops, string itemId)
    {
        if (shops == null)
            return 2500;
        if (shops.Length <= 0)
            return 2500;
        var find = shops.FirstOrDefault(x => x.itemId == itemId);
        if (find == null)
            return 2500;

        return find.prices[0].amount;
    }

    public void PlayGame(int score)
    {
        if (is_buying)
            return;
     
        StartCoroutine(play_game(score));
    }
    IEnumerator play_game(int score)
    {
        is_buying = true;
        UnityWebRequest www = UnityWebRequest.Post(url + api + "games/" + gameId + "/playgame?score=" + score, "");
        AddBearer(www);

        yield return www.SendWebRequest();

        Debug.Log(www.downloadHandler.text);

        if (www.responseCode > 200)
        {
            // error
        }
        else
        {
            var r = JsonUtility.FromJson<GetResources>(www.downloadHandler.text);
            UpdateGetResourceGraphics(r);
        }

        // hide loading
        is_buying = false;
    }

    [Button("Get Me")]
    void Test ()
    {
        StartCoroutine(get_me());
    }

    [Button("Get Score")]
    void TestScore()
    {
        StartCoroutine(get_user_score());
    }

    [Button("Get Leaderboard")]
    void Test2()
    {
        StartCoroutine(get_leaderboard());
        // test put score
        // test leaderboard
        // test get score
    }

    [Button("Test Put")]
    void TestPutScore()
    {
        PutUserScore(900, x =>
        {
            Debug.Log(x);
        });
    }
    IEnumerator get_me()
    {
        UnityWebRequest www = UnityWebRequest.Get(url + api + "me");
        AddBearer(www);

        yield return www.SendWebRequest();

        var error = string.IsNullOrEmpty(www.error) == false;
        if (error)
        {
            Debug.Log("error: " + www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            me = JsonUtility.FromJson<MeJSON>(www.downloadHandler.text);

            // show name
            var label = UIComponent.Get("PLAYER NAME").GetComponentInChildren<TextMeshProUGUI>();
            label.SetText(me.name);
        }
    }

    public IEnumerator get_leaderboard ()
    {
        // https://api.egat.dev.apollo21.asia/api/v1/games/{GameId}/rank
        UnityWebRequest www = UnityWebRequest.Get(url +api+ "games/" + gameId + "/rank");
        AddBearer(www);

        yield return www.SendWebRequest();

        Debug.Log(www.downloadHandler.text);

        try
        {
            leaderboards = JsonHelper.ParseJsonArray<LeaderboardItem[]>(www.downloadHandler.text);
        }
        catch (System.Exception)
        {
            leaderboards = new LeaderboardItem[0];
        }
       

        //var _fixed = fixJsonArray(www.downloadHandler.text);
        //leaderboard = JsonUtility.FromJson<LeaderboardJSON>(_fixed);
    }

    IEnumerator get_user_score ()
    {
        UnityWebRequest www = UnityWebRequest.Get(url + "games/" + gameId + "/score");
        AddBearer(www);
        yield return www.SendWebRequest();

        Debug.Log(www.downloadHandler.text);
    }

    [System.Serializable]
    public class NewScore
    {
        public int newScore;
    }

    [System.Serializable]
    public class NewScoreRes
    {
        public int gameId;
        public int newScore;
        public bool isHighest;
    }
    IEnumerator put_user_score(int score, System.Action<NewScoreRes> done)
    {
        var new_score = new NewScore() { newScore = score };
        var json = JsonUtility.ToJson(new_score);

        var request = new UnityWebRequest(url + api + "me/games/" + gameId + "/score", "PUT");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        AddBearer(request);
        yield return request.SendWebRequest();
        Debug.Log(request.downloadHandler.text);
        var res = JsonUtility.FromJson<NewScoreRes>(request.downloadHandler.text);
        if (done != null)
            done.Invoke(res);

        var leaderboard = GameObject.FindObjectOfType<LeaderboardUI>();
        if (leaderboard)
        {
            leaderboard.needNewData = true;
        }
    }

    public void PutUserScore(int score, System.Action<NewScoreRes> done)
    {
        StartCoroutine(put_user_score(score, done));
    }

    //IEnumerator get_user_progress ()
    //{
    //    UnityWebRequest www = UnityWebRequest.Get(url + "games/" + gameId + "/progression");
    //    AddBearer(www);
    //    yield return www.SendWebRequest();

    //    Debug.Log(www.downloadHandler.text);
    //}

    //IEnumerator put_user_progress ()
    //{
    //    var progress = new GameProgress();
    //    progress.level = Random.Range(10, 100);
    //    var string_progress = JsonUtility.ToJson(progress);

    //    var request = new UnityWebRequest(url + "games/" + gameId + "/progression", "PUT");
    //    byte[] bodyRaw = Encoding.UTF8.GetBytes(string_progress);
    //    request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
    //    request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
    //    request.SetRequestHeader("Content-Type", "application/json");
    //    AddBearer(request);
    //    yield return request.SendWebRequest();
    //    Debug.Log("Status Code: " + request.responseCode);
    //    Debug.Log(request.downloadHandler.text);
    //}

    void AddBearer (UnityWebRequest www) {
        www.SetRequestHeader("Authorization", "Bearer " + token);
    }

    //string fixJsonArray(string value)
    //{
    //    value = "{\"Items\":" + value + "}";
    //    return value;
    //}

}

[System.Serializable]
public class MeJSON
{
    public int id;
    public string name;
    public string email;
    public bool isSuspended;
    public int genderId;
    public string birthday;
    public string address;
    public string subDistrict;
    public string province;
    public string zipcode;
    public string school;
    public string lineId;
    public string facebookUrl;
    public string pictureUrl;
    public int profileTypeId;
    public string originalName;
    public string schoolId;
}

//[System.Serializable]
//public class LeaderboardJSON
//{
//    public List<LeaderboardItem> Items;
//}

[System.Serializable]
public class LeaderboardItem
{
    public int gameId;
    public string gameName;
    public int profileId;
    public string profileName;
    public int score;
}

[System.Serializable]
public class GameProgress
{
    public int level = 10;
}

[System.Serializable]
public class GetResources
{
    public int profileId;
    public Energy energy;
    public EnergyDrop[] energyDrop;
    public GreenPoint greenPoint;
    public InvItem[] items;
    public BuildingItem[] buildings;
}

[System.Serializable]
public class Energy
{
    public int currentEnergy;
    public float timeLeft;
    public int increase;
}

[System.Serializable]
public class EnergyDrop
{
    public float timeLeft;
    public int positionIndex;
}

[System.Serializable]
public class GreenPoint
{
    public int currentGreenPoint;
    public float timeLeft;
    public int increase;
}

[System.Serializable]
public class InvItem
{
    public string itemId;
    public int amount;
}

[System.Serializable]
public class BuildingItem
{
    public string itemId;
    public float timeLeft;
}

[System.Serializable]
public class ShopItem
{
    public string itemId;
    public int gameId;
    public Price[] prices;
    public int buildTime;
}

[System.Serializable]
public class Price
{
    public string itemId;
    public int amount;
}
