using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Cinemachine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanCameraControl : MonoBehaviour
{
    [Range(0f, 1f)]
    public float x, y;
    float target_x, target_y;

    public float target_fov { get; set; }

    Vector3 left, right, top, bottom, center;
    public Transform view;

    public float speedX = 1f;
    public float speedY = 1f;

    Vector3 a, b;
    Vector3 prev_mouse;

    public float doubleClickTime = 0.2f;
    float clickDownTime;

    new CinemachineVirtualCamera camera;

    public FreeLookControl control { get; set; }

    public enum ControlType
    {
        Mouse, Touch
    }

    ControlType type = ControlType.Mouse;

    Slider zoom;


    void Awake()
    {
        camera = GetComponent<CinemachineVirtualCamera>();
        control = GameObject.FindObjectOfType<FreeLookControl>();
    }

    [Button("test")]
    void Start()
    {
        center = view.position;
        left = -view.right * view.localScale.x * 0.5f;
        right = view.right * view.localScale.x * 0.5f;
        top = view.up * view.localScale.y * 0.5f;
        bottom = -view.up * view.localScale.y * 0.5f;

        target_x = x;
        target_y = y;

        target_fov = camera.m_Lens.FieldOfView;

        var zoom_ui = UIComponent.Get("ZOOM");
        if (zoom_ui)
        {
            zoom = zoom_ui.GetComponent<Slider>();
            zoom.value = target_fov;
        }

        if (Input.touchSupported)
            type = ControlType.Touch;
        else
            type = ControlType.Mouse;
    }

    void TestUpdate ()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            var target1 = GameObject.Find("target1");
            ChangeLookAt(target1.transform);

            var view1 = GameObject.Find("view1");
            ChangeView(view1.transform);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            var target2 = GameObject.Find("target2");
            ChangeLookAt(target2.transform);

            var view2 = GameObject.Find("view2");
            ChangeView(view2.transform);
        }
    }


    bool IsControlable ()
    {
        if (control.IsPause)
            return false;

        //if (MyTool.CurrentToolID != MyTool.VIEW)
        //    return false;

        return true;
    }

    void MouseUpdate()
    {
        if (EventSystem.current.currentSelectedGameObject != null)
            if (EventSystem.current.currentSelectedGameObject.layer == 5)
                return;

        if (Input.GetMouseButtonDown(0))
        {
            prev_mouse = Input.mousePosition;

            if (Time.time - clickDownTime < doubleClickTime)
            {
                target_x = 0.5f;
                target_y = 0.5f;


                var default_setting = GameObject.FindObjectOfType<DefaultObjectSetting>();
                if (default_setting)
                {
                    if (default_setting.panView)
                        ChangeLookAt(default_setting.lookAt);
                    target_fov = default_setting.fov;
                    zoom.value = target_fov;
                }
                else
                {
                    transform.DODynamicLookAt(camera.LookAt.position, 0.5f);
                }
            }
            clickDownTime = Time.time;
        }
        else if (Input.GetMouseButton(0))
        {
            // pan
            var delta = Input.mousePosition - prev_mouse;
            target_x += delta.x * speedX * Time.deltaTime;
            target_y += delta.y * speedY * Time.deltaTime;
            target_x = Mathf.Clamp(target_x, 0f, 1f);
            target_y = Mathf.Clamp(target_y, 0f, 1f);

            prev_mouse = Input.mousePosition;
        }
    }
    void TouchUpdate()
    {
        if (Input.touchCount > 1)
            return;

        MouseUpdate();
    }

    void Update()
    {
        if (IsControlable())
        {
            if (type == ControlType.Mouse)
                MouseUpdate();
            else if (type == ControlType.Touch)
                TouchUpdate();
        }

        x = Mathf.Lerp(x, target_x, 8f * Time.deltaTime);
        y = Mathf.Lerp(y, target_y, 8f * Time.deltaTime);

        a = Vector3.Lerp(left, right, x);
        b = Vector3.Lerp(bottom, top, y);

        transform.position = Vector3.Lerp(transform.position, center + a + b, 8f * Time.deltaTime);

        camera.m_Lens.FieldOfView = Mathf.Lerp(camera.m_Lens.FieldOfView, target_fov, 4f * Time.deltaTime);

        //TestUpdate();
    }

    public void ChangeView(Transform new_view)
    {
        view = new_view;

        center = view.position;
        left = -view.right * view.localScale.x * 0.5f;
        right = view.right * view.localScale.x * 0.5f;
        top = view.up * view.localScale.y * 0.5f;
        bottom = -view.up * view.localScale.y * 0.5f;

        x = 0.5f;
        y = 0.5f;
        target_x = x;
        target_y = y;
    }

    public void ChangeLookAt(Transform new_lookat)
    {
        camera.LookAt = new_lookat;
        transform.DODynamicLookAt(camera.LookAt.position, 0.5f);
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawSphere(transform.position + left, 0.1f);
    //    Gizmos.DrawSphere(transform.position + right, 0.1f);
    //    Gizmos.DrawSphere(transform.position + top, 0.1f);
    //    Gizmos.DrawSphere(transform.position + bottom, 0.1f);

    //    Gizmos.color = Color.green;
    //    Gizmos.DrawSphere(transform.position + a, 0.1f);

    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawSphere(transform.position + b, 0.1f);
    //}
}
