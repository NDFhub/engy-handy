using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWarning : MonoBehaviour
{
    public GameObject warning;

    float _next;

    void Start()
    {
        if (warning)
            warning.SetActive(false);
    }

    void Update()
    {
        Check();
    }

    void Check()
    {
        if (warning == null)
            return;

        if (Time.time < _next)
            return;

        if (Screen.height > Screen.width)
        {
            // show warning
            if (warning.activeSelf == false)
                warning.SetActive(true);

            _next = Time.time + 0.1f;
        }
        else
        {
            if (warning.activeSelf)
                warning.SetActive(false);

            _next = Time.time + 1;
        }

    }

    private void OnGUI()
    {
        GUILayout.Box(Screen.width + ", " + Screen.height);
    }
}
