using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;

public class MainRoomScript : MonoBehaviour
{
    [System.Serializable]
    public class Unit
    {
        public eUnit id;
        public List<MainRoomUnit> locations;

        public MainRoomUnit random(List<string> used_locations)
        {
            var pools = locations.ToDictionary(x => x.location, x => x);

            foreach (var location in used_locations)
            {
                if (pools.ContainsKey(location))
                {
                    pools.Remove(location);
                }
            }

            if (pools.Count <= 0)
                return null;

            var rand = Random.Range(0, pools.Count);
            return pools.ElementAt(rand).Value;
        }
    }

    public enum eUnit : int
    {
        Friend1 = 0, Friend2 = 1, Friend3 = 2, Friend4 = 3, Engy, Thomas, Maria
    }

    public List<string> usedLocations;
    public Dictionary<eUnit, Unit> dict;
    public MainRoomUnit[] all;

    bool init = false;

    IEnumerator Start()
    {
        init = false;
        all = GameObject.FindObjectsOfType<MainRoomUnit>(true);
        dict = new Dictionary<eUnit, Unit>();
        // hide all
        foreach (var item in all)
        {
            if (dict.ContainsKey(item.id) == false)
                dict.Add(item.id, new Unit() { id = item.id, locations = new List<MainRoomUnit>() });

            dict[item.id].locations.Add(item);
        }

        yield return new WaitUntil(() => API.singleton.IsInited);

        RandomPosition();

        init = true;
    }

    private void OnEnable()
    {
        if (init)
            RandomPosition();
    }

    [Button("random")]
    public void RandomPosition()
    {
        foreach (var item in all)
        {
            item.gameObject.SetActive(false);
        }

        usedLocations = new List<string>();

        var unit_pool = new List<eUnit>();
        unit_pool.Add(eUnit.Engy);
        if (API.singleton.hasThomasAura)
            unit_pool.Add(eUnit.Thomas);
        if (API.singleton.hasMariaAura)
            unit_pool.Add(eUnit.Maria);

        // rand unit
        var unit_count = Random.Range(1, unit_pool.Count + 1);
        var unit_remove_count = unit_pool.Count - unit_count;
        for (int i = 0; i < unit_remove_count; i++)
        {
            var remove_rand = Random.Range(0, unit_pool.Count);
            unit_pool.RemoveAt(remove_rand);
        }

        foreach (var item in unit_pool)
        {
            var friend = dict[item];
            var rand_unit = friend.random(usedLocations);
            rand_unit.gameObject.SetActive(true);

            usedLocations.Add(rand_unit.location);
        }

        var friend_pool = new List<eUnit>();
        friend_pool.Add(eUnit.Friend1);
        friend_pool.Add(eUnit.Friend2);
        friend_pool.Add(eUnit.Friend3);
        friend_pool.Add(eUnit.Friend4);

        var friend_count = Random.Range(1, 5);
        var remove_count = 4 - friend_count;
        for (int i = 0; i < remove_count; i++)
        {
            var remove_rand = Random.Range(0, friend_pool.Count);
            friend_pool.RemoveAt(remove_rand);
        }

        // rand friends
        foreach (var item in friend_pool)
        {
            var friend = dict[item];
            var rand_friend = friend.random(usedLocations);
            rand_friend.gameObject.SetActive(true);

            usedLocations.Add(rand_friend.location);
        }
    }
}
