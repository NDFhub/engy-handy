using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolObject : MonoBehaviour
{
    public string id;
    // public string label;
    // public Sprite icon;

    public ToolButton button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<ToolButton>();
            return _button;
        }
    }
    ToolButton _button;
}
