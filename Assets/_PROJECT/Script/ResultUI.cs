using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class ResultUI : MonoBehaviour
{
    public GameObject ui;
    public CanvasGroup canvas;

    public TextMeshProUGUI text;
    public TextMeshProUGUI total;

    public TextMeshProUGUI practiceLabel;
    public TextMeshProUGUI challengeLabel;

    string PRACTICE_FORMAT;
    string CHALLENGE_FORMAT;


    void Start()
    {
        PRACTICE_FORMAT = practiceLabel.text;
        CHALLENGE_FORMAT = challengeLabel.text;
    }

    // call by logic
    public void Show()
    {
        //if (Main.singleton.selectGame.easy)
        //    Main.singleton.score.AddBonusTimeScore();

        //text.SetText(Main.singleton.score.ToString());
        //var total_score = Main.singleton.score.GetTotal();
        //total.SetText(total_score.ToString());
        //if (API.singleton)
        //    API.singleton.PutUserScore(total_score, (res) => { });
        //canvas.alpha = 0;
        //ui.SetActive(true);
        //DOTween.To(() => canvas.alpha, x => canvas.alpha = x, 1f, 0.5f);
        Main.singleton.timer.Hide();

        // to result state
        var fsm = Main.singleton.GetComponent<PlayMakerFSM>();
        fsm.SendEvent("END");


        var practice_root = UIComponent.Get("PRACTICE RESULT ROOT");
        var challenge_root = UIComponent.Get("CHALLENGE RESULT ROOT");
        if (practice_root)
            practice_root.gameObject.SetActive(Main.singleton.selectGame.easy);
        
        if (challenge_root)
            challenge_root.gameObject.SetActive(Main.singleton.selectGame.easy == false);
        

        if (Main.singleton.selectGame.easy)
        {
            var label = Main.singleton.selectGame.easyGameData.label;
            var result = string.Format(PRACTICE_FORMAT, label);
            practiceLabel.SetText(result);
            practiceLabel.gameObject.SetActive(true);
        }
        else
        {
            var count = Main.singleton.score.GetObjectCount();
            var score = Main.singleton.score.GetTotal();

            if (API.singleton.hasMariaAura)
            {
                score = (int)(score * 1.5f);
            }

            var result = string.Format(CHALLENGE_FORMAT, count, score);
            challengeLabel.SetText(result);
            challengeLabel.gameObject.SetActive(true);

            API.singleton.PutUserScore(score, x => { });
            API.singleton.PlayGame(score);
        }
    }
}
