using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ObjectiveText : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI label;
    public Toggle toggle;
    [SerializeField] Image line;
    public ObjectiveLine data;
    public Image background1;
    public Image background2;

    void Start()
    {

    }

    public void SetText(string s)
    {
        if (label)
            label.SetText(s);
    }

    public void SetDone(bool done)
    {
        if (line)
            line.gameObject.SetActive(done);
        if (toggle)
            toggle.isOn = done;

        if (background1)
            background1.gameObject.SetActive(false);
        if (background2)
            background2.gameObject.SetActive(true);
    }

    public void SetData(ObjectiveLine data)
    {
        this.data = data;
        data.bindUI = this;
    }

}
