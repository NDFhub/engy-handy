using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

public class NeedTool : MonoBehaviour
{
    [ValueDropdown("tool_list")]
    public List<string> tools;

    void Start()
    {
        var list = GameObject.FindObjectsOfType<ToolObject>(true);
        foreach (var item in list)
            item.gameObject.SetActive(false);

        foreach (var item in tools)
        {
            var t = list.FirstOrDefault(x => x.id == item);
            if (t)
                t.gameObject.SetActive(true);
        }
    }

    static IEnumerable tool_list()
    {
        var tools = GameObject.FindObjectsOfType<ToolData>(true);
        return tools.Select(x => x.id);
    }
}
