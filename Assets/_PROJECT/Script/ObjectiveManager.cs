using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System;
using UnityEngine.UI;

public class ObjectiveManager : MonoBehaviour
{
    public static ObjectiveManager singleton;
    public GameObject ui;
    public Transform root;
    RectTransform layout;
    public GameObject main, sub;

    public List<ObjectiveText> instanced;
    List<ObjectiveLine> objectives;

    public Action RunTest;
    Coroutine coroutine;


    void Start()
    {
        singleton = this;
        instanced = new List<ObjectiveText>();
        objectives = new List<ObjectiveLine>();
        layout = root.GetComponent<RectTransform>();
    }

    public static void Set(ObjectiveLine[] lines)
    {
        if (singleton.coroutine != null)
            singleton.StopCoroutine(singleton.coroutine);

        singleton.clear_instanced();

        singleton.objectives.Clear();
        singleton.objectives.AddRange(lines);

        singleton.coroutine = singleton.StartCoroutine(singleton.queue_ui());
    }

    public void clear_instanced()
    {
        foreach (var item in instanced)
        {
            if (item)
                GameObject.Destroy(item.gameObject);
        }

        instanced.Clear();
    }

    IEnumerator queue_ui()
    {
        yield return StartCoroutine(create(objectives[0]));

        objectives.RemoveAt(0);

        if (objectives.Count > 0)
            coroutine = StartCoroutine(queue_ui());
        else
            RunTest.Invoke();
    }

    IEnumerator create(ObjectiveLine line)
    {
        //var done = false;
        //GameObject go = null;
        //AssetReferenceGameObject asset = null;
        //if (line.sub)
        //    asset = sub;
        //else
        //    asset = main;

        //asset.InstantiateAsync().Completed += x =>
        //{
        //    go = x.Result;
        //    done = true;
        //};
        //var wait = new WaitUntil(() => done);

        //yield return wait;

        GameObject go = null;
        if (line.sub)
        {
            go = GameObject.Instantiate<GameObject>(sub);
        }
        else
        {
            go = GameObject.Instantiate<GameObject>(main);
        }

        yield return null;

        var script = go.GetComponent<ObjectiveText>();
        script.SetData(line);
        script.SetText(line.text);
        script.SetDone(false);

        go.transform.SetParent(root, false);

        if (line.InGroup)
            go.SetActive(line.mainGroup);

        LayoutRebuilder.ForceRebuildLayoutImmediate(layout);
        instanced.Add(script);
    }

    public static void Rebuild ()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(singleton.layout);
    }
}
