using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    string Format = "{0:00}:{1:00}";
    float starttime;
    float endtime;
    public TextMeshProUGUI text;
    public GameObject panel;
    bool easy;

    // Start is called before the first frame update
    void Start()
    {
        Hide();
    }
    public void Show(bool easy = true)
    {
        this.easy = easy;
        starttime = Time.time;
        endtime = Time.time + 300;

        panel.SetActive(true);
        enabled = true;
    }

    public void Hide()
    {
        panel.SetActive(false);
        enabled = false;
    }

    public float GetPlayTime()
    {
        return (Time.time - starttime) * 1000;
    }

    // Update is called once per frame
    void Update()
    {
        if (easy)
        {
            var t = GetPlayTime();
            text.SetText(FormatTime(t, Format));
        }
        else
        {
            var left = (endtime - Time.time) * 1000;
            if (left <= 0)
            {
                left = 0;
                // show result
                Main.singleton.result.Show();
                Hide();
            }
            text.SetText(FormatTime(left, Format));
        }
    }

    public static string FormatTime(float time, string format)
    {
        int minutes = (int)time / 60000;
        int seconds = (int)time / 1000 - 60 * minutes;

        return string.Format(format, minutes, seconds);
    }
}
