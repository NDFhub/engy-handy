using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideGameplayUI : MonoBehaviour
{
    public GameObject UI;
    private void OnEnable()
    {
        UI.SetActive(false);
    }
    private void OnDisable()
    {
        UI.SetActive(true);
    }

}
