using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager singleton;
    public List<Score> list;
    public float multiplay = 1f;

    public Score wrong;
    public Score timer;

    public TextMeshProUGUI realtime;

    void Start()
    {
        singleton = this;
        list = new List<Score>();
    }

    public void Clear()
    {
        if (list == null)
            list = new List<Score>();

        foreach (var item in list)
            GameObject.Destroy(item.gameObject);

        list.Clear();
        realtime.SetText("0");
    }

    void UpdateRealtimeScore()
    {
        var score = GetTotal();
        realtime.SetText(score.ToString("N0"));
    }

    public void AddScore(Score s)
    {
        var new_score = new GameObject("score" + s.label);
        var score = new_score.AddComponent<Score>();
        score.id = s.id;
        score.label = s.label;
        score.order = s.order;
        score.score = s.score;

        list.Add(score);
        UpdateRealtimeScore();
    }

    public void AddWrong(PopupTextContainer failed)
    {
        Main.singleton.popup.AddError(failed.text);
        
        AddScore(wrong);

        UpdateRealtimeScore();
    }

    public void AddBonusTimeScore()
    {
        var a = GameObject.Instantiate(timer);
        var playtime = Main.singleton.timer.GetPlayTime();
        a.label = Timer.FormatTime(playtime, timer.label);
        a.score = 100;

        list.Add(a);
    }

    public override string ToString()
    {
        var temp = "";
        var groups = list.GroupBy(x => x.id);
        var sorted = groups.OrderBy(x => x.First().order);
        foreach (var g in sorted)
        {
            var count = g.Count();
            var first = g.First();
            var score = g.Sum(x => x.score);
            var line = first.label;
            if (count > 1)
                line += " (" + count + ")";
            line += ": " + (int)(score * multiplay);
            temp += line + "\n";
            //Debug.Log(line);
        }
        if (temp.Length > 3)
            temp = temp.Substring(0, temp.Length - 1); // remove last \n
        //Debug.Log(temp);
        return temp;
    }

    public int GetTotal()
    {
        var result = (int)list.Sum(x => x.score * multiplay);
        result = Mathf.Max(0, result);
        return result;
    }

    public int GetObjectCount()
    {
        var items = list.Where(x => x.id == "clear");
        return items.Count();
    }
}
