using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

public class TestState : MonoBehaviour
{
    public PlayMakerFSM fsm;


    [ShowIf("@this.fsm != null")]
    public bool checkState;
    [ShowIf("@this.checkState")]
    [ValueDropdown("get_state")]
    public string stateName;

    [ShowIf("@this.fsm != null")]
    public bool checkVariable;
    [ShowIf("@this.checkVariable")]
    [ValueDropdown("get_var")]
    [HorizontalGroup("Group 2")]
    public string variableName;
    [ShowIf("@this.checkVariable")]
    [HorizontalGroup("Group 2")]
    [HideLabel]
    public bool isTrue;

    bool CheckState()
    {
        if (checkState)
        {
            if (fsm)
            {
                if (fsm.ActiveStateName == stateName)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        return true;
    }

    bool CheckBool()
    {
        if (checkVariable)
        {
            if (fsm)
            {
                if (fsm.FsmVariables.GetFsmBool(variableName).Value == isTrue)
                    return true;
                else
                    return false;
            }
        }

        return true;
    }

    IEnumerable get_state()
    {
        if (fsm == null)
            return "";

        return fsm.FsmStates.Select(x => x.Name);
    }

    IEnumerable get_var()
    {
        if (fsm == null)
            return "";

        return fsm.FsmVariables.BoolVariables.Select(x => x.Name);
    }
}
