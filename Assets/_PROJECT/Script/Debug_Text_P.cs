using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

    [ActionCategory("Game")]
    [ActionTarget(typeof(PlayMakerFSM), "gameObject,fsmName")]
    public class Debug_Text_P : FsmStateAction
    {
        
        public PopupTextContainer failedText;
        
        public override void OnEnter()
        {
            DebugFail();
        }

        void DebugFail()
        {
            if (failedText)
                ScoreManager.singleton.AddWrong(failedText);
           // Fsm.SetState(Fsm.PreviousActiveState.Name);
            Finish();
        }
    }
}