using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ShopScript : MonoBehaviour
{
    int money
    {
        get
        {
            return API.singleton.score;
        }
    }
    int count = 1;
    int unitPrice
    {
        get
        {
            return API.singleton.unitPrice;
        }
    }
    public TextMeshProUGUI number;
    public TextMeshProUGUI price;
    public Button buyButton;

    void Start()
    {
        UpdateGraphics();
    }

    public void Minus()
    {
        count--;
        if (count < 1)
            count = 1;

        UpdateGraphics();
    }
    public void Add()
    {
        count++;
        if (count > 10)
            count = 10;

        UpdateGraphics();
    }

    public void Buy()
    {
        // call api
        API.singleton.Buy("screwdriver_handy", count);
    }

    void UpdateGraphics()
    {
        var p = GetTotalPrice();
        number.SetText(count.ToString());
        price.SetText(p.ToString());
        buyButton.interactable = money >= p;
    }

    int GetTotalPrice()
    {
        return count * unitPrice;
    }
}
