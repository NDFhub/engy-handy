using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class Main : MonoBehaviour
{
    public static Main singleton;

    public SelectGameUI selectGame;
    public ObjectiveManager objective;
    public PositionManager position;
    public ResultUI result;

    public ScoreManager score;

    public PopupManager popup;
    public FreeLookControl freelook;

    public Timer timer;

    public List<AssetReferenceGameObject> list;

    public GameObject clothFX;
    public GameObject wetclothFX;
    public GameObject BRUSHFX;
    public GameObject WATERFX;
    public GameObject VINEGARFX;
    public GameObject LADLEFX;
    public GameObject SPONGEFX;
    
    
    AudioSource audioSource;
    [SerializeField] AudioClip clean;

    private void Start()
    {
        singleton = this;

        // set default
        //InitUI();
        // start game
        //selectGame.Show();
        audioSource = GetComponent<AudioSource>();
    }

    public void CreateClothFX(Vector3 position)
    {
        var go = GameObject.Instantiate<GameObject>(clothFX);
        go.transform.position = position;
        if (AudioListener.pause == false)
            audioSource.PlayOneShot(clean);
    }
    
    public void CreateWetClothFX(Vector3 position)
    {
        var go = GameObject.Instantiate<GameObject>(wetclothFX);
        go.transform.position = position;
        if (AudioListener.pause == false)
            audioSource.PlayOneShot(clean);
    }
    
    public void CreateBrushFX(Vector3 position)
    {
        Vector3 newposition = new Vector3(0.05f, 0.01f, 0.02f);
        var go = GameObject.Instantiate<GameObject>(BRUSHFX);
        go.transform.position = position;
        /*var go2 = GameObject.Instantiate<GameObject>(BRUSHFX);
        go2.transform.position = position+newposition;*/
        if (AudioListener.pause == false)
            audioSource.PlayOneShot(clean);
    }
    
    public void CreateWaterFX(Vector3 position)
    {
        var go = GameObject.Instantiate<GameObject>(WATERFX);
        go.transform.position = position;
        if (AudioListener.pause == false)
            audioSource.PlayOneShot(clean);
    }
    
    public void CreateVinegarFX(Vector3 position)
    {
        var go = GameObject.Instantiate<GameObject>(VINEGARFX);
        go.transform.position = position;
        if (AudioListener.pause == false)
            audioSource.PlayOneShot(clean);
    }
    
    public void CreateLADLEFX(Vector3 position)
    {
        var go = GameObject.Instantiate<GameObject>(LADLEFX);
        go.transform.position = position;
        if (AudioListener.pause == false)
            audioSource.PlayOneShot(clean);
    }
    public void CreateSPONGEFX(Vector3 position)
    {
        var go = GameObject.Instantiate<GameObject>(SPONGEFX);
        go.transform.position = position;
        if (AudioListener.pause == false)
            audioSource.PlayOneShot(clean);
    }

    //public void InitUI()
    //{
    //    selectGame.ui.SetActive(false);
    //    result.ui.SetActive(false);
    //}

    public void StartEasyGame(GameObject go)
    {
        score.multiplay = 1f;
        selectGame.easy = true;

        if (selectGame.current != null)
            Addressables.ReleaseInstance(selectGame.current);
        //GameObject.Destroy(selectGame.current);

        selectGame.current = go;

        objective.ui.SetActive(selectGame.easy); // set group

        selectGame.ui.SetActive(false); // dont use

        position.Init();
        //InitUI();
        score.Clear(); // set group
        timer.Show(); // set group
        MyTool.singleton.Deselect();
    }

    public void SetEasyGameData(PracticeModeData data)
    {
        selectGame.easyGameData = data;
    }

    public void StartHardGame(List<AssetReferenceGameObject> objs, float multiply = 1f)
    {
        UIComponent.Get("LOADING").gameObject.SetActive(true);

        score.multiplay = multiply;
        selectGame.easy = false;
        list = objs;

        if (selectGame.current != null)
            Addressables.ReleaseInstance(selectGame.current);
        //GameObject.Destroy(selectGame.current);

        // instanced go
        var rand = Random.Range(0, list.Count);
        last_random = rand;
        list[rand].InstantiateAsync().Completed += x =>
        {
            selectGame.current = x.Result;

            objective.ui.SetActive(selectGame.easy);
            selectGame.ui.SetActive(false);

            position.Init();
            //InitUI();
            score.Clear();
            timer.Show(selectGame.easy);

            UIComponent.Get("LOADING").gameObject.SetActive(false);
        };
        MyTool.singleton.Deselect();
    }

    int last_random;
    bool createing_next;
    public void NextItem()
    {
        if (createing_next)
            return;
        
        StartCoroutine(delay());
    }


    IEnumerator delay ()
    {
        UIComponent.Get("LOADING").gameObject.SetActive(true);

        createing_next = true;
        if (selectGame.current != null)
            //GameObject.Destroy(selectGame.current);
            Addressables.ReleaseInstance(selectGame.current);
        // instanced go

        yield return new WaitForSeconds(1f);

        var rand = Random.Range(0, list.Count);
        if (list.Count > 1)
        {
            while (last_random == rand)
            {
                rand = Random.Range(0, list.Count);
            }
        }

        list[rand].InstantiateAsync().Completed += x =>
        {
            selectGame.current = x.Result;
            position.Init();

            last_random = rand;
            createing_next = false;

            UIComponent.Get("LOADING").gameObject.SetActive(false);
        };
    }

    public void Replay()
    {
        var fsm = Main.singleton.GetComponent<PlayMakerFSM>();
        
        if (selectGame.easy)
        {
            fsm.SetState("practice play");
            var asset = selectGame.easyGameData.GetRandom();
            UIComponent.Get("LOADING").gameObject.SetActive(true);
            asset.InstantiateAsync().Completed += x =>
            {
                StartEasyGame(x.Result);
                UIComponent.Get("LOADING").gameObject.SetActive(false);
            };
        }
        else
        {
            fsm.SetState("challenge play");
            StartHardGame(list, score.multiplay);
        }
    }
}
