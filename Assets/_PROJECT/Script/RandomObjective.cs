using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomObjective : MonoBehaviour
{
    void Start()
    {
        var objs = new List<ObjectiveLine>();
        foreach (Transform child in transform)
        {
            var obj = child.GetComponent<ObjectiveLine>();
            objs.Add(obj);
        }
        var count = objs.Count;
        var rand = Random.Range(0, count);
        for (int i = 0; i < count; i++)
        {
            if (i == rand)
            {
                objs[i].gameObject.SetActive(true);
            }
            else
            {
                if (objs[i].fsm != null)
                    GameObject.Destroy(objs[i].fsm.gameObject);

                GameObject.Destroy(objs[i].gameObject);
            }
        }
    }
}
