using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

public class ClickWithTool : MonoBehaviour
{

    [OnValueChanged("on_changed")]
    [ValueDropdown("tool_list")]
    public string tool;
    PlayMakerFSM fsm;

    // add condition here

    /// <summary>
    /// call from ClickObject.cs
    /// </summary>
    public void OnClick()
    {
        if (fsm == null)
            fsm = transform.parent.GetComponent<PlayMakerFSM>();

        if (MyTool.CurrentToolID == MyTool.VIEW)
            return;

        if (MyTool.CurrentToolID != tool)
            return;

        if (!fsm)
            return;

        var event_name = GetTool;
        var state = fsm.FsmStates.FirstOrDefault(x => x.Name == fsm.ActiveStateName);
        var transition = state.Transitions.FirstOrDefault(x => x.EventName == event_name);

        //Debug.Log("click "+transform.name);
        if (transition != null)
        {
            fsm.SendEvent(event_name);
            
            MyTool.singleton.Used(this);

            if (tool == MyTool.CLOTH)
            {
                Main.singleton.CreateClothFX(transform.position);
            }else
            if(tool == MyTool.WET_CLOTH)
            {
                Main.singleton.CreateWetClothFX(transform.position);
            }else
            if(tool == MyTool.BRUSH)
            { 
                Main.singleton.CreateBrushFX(transform.position);
            }else
            if(tool == MyTool.WATER)
            {
                Main.singleton.CreateWaterFX(transform.position);
            }
            else
            if(tool == MyTool.VINEGAR)
            {
                Main.singleton.CreateVinegarFX(transform.position);
            }else
            if(tool == MyTool.LADLE)
            {
                Main.singleton.CreateLADLEFX(transform.position);
            }else
            if(tool == MyTool.SPONGE)
            {
                Main.singleton.CreateSPONGEFX(transform.position);
            }




            StartCoroutine(update_position());
        }
        else
        {
            // failed no transition for event
            Debug.LogError("using wrong tool");
        }

    }

    IEnumerator update_position ()
    {
        yield return null;

        // after transition
        var pos = fsm.GetComponent<ObjectPosition>();
        if (pos)
        {
            pos.UpdatePosition();
        }
    }

    string GetTool
    {
        get
        {
            if (string.IsNullOrEmpty(tool))
                return MyTool.VIEW;

            return tool;
        }
    }

    public bool IsActive()
    {
        if (gameObject.activeSelf == false)
            return false;

        if (enabled == false)
            return false;

        return true;
    }

#if UNITY_EDITOR

    void on_changed()
    {
        name = "WITH " + tool;
    }

    static IEnumerable tool_list()
    {
        var tools = GameObject.FindObjectsOfType<ToolData>(true);
        return tools.Select(x => x.id);
    }

#endif
}
