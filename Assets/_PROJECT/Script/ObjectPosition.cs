using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;
using DG.Tweening;

public class ObjectPosition : MonoBehaviour
{
    [ValueDropdown("get_state")]
    public string state;

    public string group;

    PlayMakerFSM fsm;

    IEnumerable<ObjectPosition> list;

    private void Start()
    {
        if (string.IsNullOrEmpty(group))
        {
            group = name;
        }
    }

    // call from playmaker
    public void UpdatePosition()
    {
        if (list == null)
            list = PositionManager.singleton.list.Where(x => x.group == this.group);

        if (fsm == null)
            fsm = GetComponent<PlayMakerFSM>();

        var current = fsm.ActiveStateName;
        var pos = list.FirstOrDefault(x => x.state == current);
        if (pos)
        {
            // dot tween
            TweenTo(pos);
        }

        // looking for group sync state
        var group = GetComponent<GroupSyncState>();
        if (group)
            group.Sync(current);
    }

    ObjectSlot[] slots;
    void TweenTo(ObjectPosition to)
    {
        transform.DOLocalMove(to.transform.localPosition, 0.5f).OnComplete(() =>
        {
            // try auto check object slot
            if (slots == null)
            {
                slots = GameObject.FindObjectsOfType<ObjectSlot>();
            }

            foreach (var item in slots)
            {
                item.UpdateSlot();
            }
        });
        //transform.DOLocalRotate(to.transform.localEulerAngles, 0.5f);
        transform.DOLocalRotateQuaternion(to.transform.localRotation, 0.5f);
    }

#if UNITY_EDITOR
    IEnumerable get_state()
    {
        var all = GameObject.FindObjectsOfType<ObjectPosition>();
        var list = all.Where(x => x.name == name);
        var main = list.FirstOrDefault(x => x.GetComponent<PlayMakerFSM>() != null);
        if (main == null)
            return "";

        var fsm = main.GetComponent<PlayMakerFSM>();
        if (!fsm)
            return "";

        return fsm.FsmStates.Select(x => x.Name);
    }

    IEnumerable get_var()
    {
        if (fsm == null)
            return "";

        //if (fsm.FsmVariables.BoolVariables.Length <= 0)
        //    return "";

        return fsm.FsmVariables.BoolVariables.Select(x => x.Name);
    }
#endif
}
