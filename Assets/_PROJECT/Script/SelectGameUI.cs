using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectGameUI : MonoBehaviour
{
    public GameObject ui;
    public GameObject current;
    public bool easy;

    public PracticeModeData easyGameData;

    void Start()
    {

    }

    public void Show ()
    {
        ui.SetActive(true);
    }

    public void CreateNewGame (GameObject new_game, bool easy)
    {
        if (current != null)
            GameObject.Destroy(current);

        current = new_game;

        this.easy = easy;
        var objective = GameObject.FindObjectOfType<ObjectiveManager>();
        objective.ui.SetActive(easy);

        ui.SetActive(false); // auto hide ui
        Main.singleton.position.Init();
        //Main.singleton.InitUI();
        Main.singleton.score.Clear();
        Main.singleton.timer.Show();
    }


}
