using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingUI : MonoBehaviour
{
    public Transform circle;
    Vector3 axis = new Vector3(0, 0, 1);
    float speed = 360;

    void Start()
    {
        
    }

    void Update()
    {
        circle.Rotate(axis, speed * Time.deltaTime);
    }
}
