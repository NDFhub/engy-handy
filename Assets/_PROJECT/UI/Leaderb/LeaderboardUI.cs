using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LeaderboardUI : MonoBehaviour
{
    public LeaderboardSlotUI prefab;
    public Transform root;
    public GameObject ui;

    List<GameObject> list;

    //public LeaderboardSlotUI me;

    /// <summary>
    /// set to true after put new score
    /// </summary>
    public bool needNewData { get; set; }
    bool init;

    private void OnEnable()
    {
        if (init == false)
        {
            init = true;
            needNewData = true;
        }

        Show(needNewData);
    }

    IEnumerator create_ui()
    {
        // show loading
        UIComponent.Get("LOADING").gameObject.SetActive(true);

        yield return new WaitUntil(() => API.singleton.IsInited);

        if (needNewData == false)
        {
            UIComponent.Get("LOADING").gameObject.SetActive(false);
            yield break;
        } 

        yield return StartCoroutine(API.singleton.get_leaderboard());

        clean();

        int order = 0;
        foreach (var item in API.singleton.leaderboards)
        {
            var go = GameObject.Instantiate<LeaderboardSlotUI>(prefab);
            go.transform.SetParent(root, false);
            go.Set(item, order);

            go.gameObject.SetActive(true);
            list.Add(go.gameObject);

            yield return null;

            order++;
        }

        //var my = API.singleton.leaderboards.FirstOrDefault(x => x.profileId == API.singleton.res.profileId);
        //if (my != null)
        //{
        //    me.Set(my, 0);
        //}

        // hide loading
        UIComponent.Get("LOADING").gameObject.SetActive(false);

        needNewData = false;
    }

    public void Show(bool need_new_data)
    {
        needNewData = need_new_data;

        ui.SetActive(true);

        StartCoroutine(create_ui());
    }

    void clean()
    {
        if (list == null)
            list = new List<GameObject>();

        foreach (var item in list)
        {
            if (item == null)
                continue;

            GameObject.Destroy(item);
        }

        list.Clear();
    }

}
