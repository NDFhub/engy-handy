using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LeaderboardSlotUI : MonoBehaviour
{
    public int ordinal;
    public Sprite[] sprites;

    public TextMeshProUGUI text;
    public TextMeshProUGUI score;
    public Image bg;

    const string N0 = "N0";

    void Start()
    {

    }

    public void Set(LeaderboardItem item, int order)
    {
        var is_me = API.singleton.me.name == item.profileName;
        ordinal = order;
        text.SetText(item.profileName);
        score.SetText(item.score.ToString(N0));
        bg.sprite = GetSprite(order, is_me);
    }

    Sprite GetSprite(int order, bool is_me)
    {
        if (order == 0)
        {
            return sprites[0];
        }
        else if (order == 1)
        {
            return sprites[1];
        }
        else if (order == 2)
        {
            return sprites[2];
        }
        else if (is_me)
        {
            return sprites[4]; // me
        }
        else
        {
            return sprites[3]; // other
        }
    }
}
