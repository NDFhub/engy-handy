using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class PracticeModeData : MonoBehaviour
{
    public string label;
    public Sprite icon;
    public AssetReferenceGameObject[] games;

    void Start()
    {

    }

    public AssetReferenceGameObject GetRandom()
    {
        var rand = Random.Range(0, games.Length);
        return games[rand];
    }

}
