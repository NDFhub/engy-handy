using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickSound : MonoBehaviour
{
    public AudioClip clip;
    [Range(0f, 1f)]
    public float volume = 1f;
    static new Camera camera;

    void Start()
    {
        var button = GetComponent<Button>();
        if (button)
        {
            button.onClick.AddListener(PlaySound);
        }
    }

    public void PlaySound()
    {
        if (camera == null)
        {
            camera = Camera.main;
        }

        AudioSource.PlayClipAtPoint(clip, camera.transform.position, volume);
    }
}

