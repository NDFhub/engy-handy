using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    static List<Pause> list;

    private void OnEnable()
    {
        init();
        clean();

        if (list.Contains(this) == false)
            list.Add(this);

        UpdatePause();
    }

    private void OnDisable()
    {
        init();

        if (list.Contains(this))
            list.Remove(this);

        clean();
        UpdatePause();
    }

    void UpdatePause ()
    {
        if (list.Count > 0)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    void init()
    {
        if (list == null)
            list = new List<Pause>();
    }

    void clean()
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == null)
                list.RemoveAt(i--);
        }
    }
}
