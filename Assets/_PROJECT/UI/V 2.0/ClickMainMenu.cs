using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickMainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var button = GetComponent<Button>();
        if (button)
        {
            button.onClick.AddListener(Click);
        }
    }

    void Click()
    {
        var fsm = Main.singleton.GetComponent<PlayMakerFSM>();
        fsm.SendEvent("MAIN");

        // destroy old game instanced
        if (Main.singleton.selectGame.current)
        {
            GameObject.Destroy(Main.singleton.selectGame.current);
        }
    }
}
