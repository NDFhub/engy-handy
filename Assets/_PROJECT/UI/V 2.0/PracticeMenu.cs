using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using System.Linq;

public class PracticeMenu : MonoBehaviour
{
    public AssetReference prefab;
    public Transform root;
    public Transform dataRoot;

    public List<PracticeModeData> list;
    List<Toggle> toggles;

    public PracticeModeData current;


    IEnumerator Start()
    {
        UIComponent.Get("LOADING").gameObject.SetActive(true);
        list = new List<PracticeModeData>();
        list.AddRange(dataRoot.GetComponentsInChildren<PracticeModeData>());

        toggles = new List<Toggle>();
        foreach (var item in list)
        {
            prefab.InstantiateAsync(root).Completed += x =>
            {
                var button = x.Result.GetComponent<PracticeItemSelectButton>();
                button.data = item;
                button.Init();
                toggles.Add(button.GetComponent<Toggle>());
                //Debug.Log("created button: " + item.icon.name);
            };
        }

        yield return new WaitUntil(() => list.Count == toggles.Count);

        if (toggles.Count > 0)
            toggles[0].isOn = true;

        UIComponent.Get("LOADING").gameObject.SetActive(false);
    }

    public void ClickPlay()
    {
        var first = toggles.FirstOrDefault(x => x.isOn);
        if (first)
        {
            current = first.GetComponent<PracticeItemSelectButton>().data;
            var game = current.GetRandom();
            UIComponent.Get("LOADING").gameObject.SetActive(true);
            game.InstantiateAsync().Completed += x =>
            {
                Main.singleton.StartEasyGame(x.Result);
                Main.singleton.SetEasyGameData(current);

                UIComponent.Get("LOADING").gameObject.SetActive(false);
            };
            // play
        }
    }
}
