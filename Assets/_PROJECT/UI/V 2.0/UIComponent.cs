using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UIComponent : MonoBehaviour
{
    static bool init = false;
    public static Dictionary<string, UIComponent> dict;
    public string id;
    public Group[] groups;

    public enum Group
    {
        MAIN_MENU, PRACTICE_MENU, PRACTICE_GAMEPLAY, CHALLENGE_MENU, CHALLENGE_GAMEPLAY, RESULT, LEADERBOARD, SHOP ,TUTORIAL
    }

    private void Start()
    {
        //if (init == false || dict == null)
        //{
        //    init = true;
        //    NewCache();
        //}
    }

    public static void NewCache ()
    {
        var list = GameObject.FindObjectsOfType<UIComponent>(true);
        dict = list.ToDictionary(x => x.id, x => x);
    }

    public static UIComponent Get(string id)
    {
        if (init == false || dict == null)
        {
            init = true;
            NewCache();
        }

        if (dict.ContainsKey(id))
            return dict[id];
        else
            return null;
    }

    public void Show(bool show)
    {
        gameObject.SetActive(show);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }

}
