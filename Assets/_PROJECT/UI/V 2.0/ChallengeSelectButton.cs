using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChallengeSelectButton : MonoBehaviour
{
    public GameObject noselect;
    public GameObject select;
    public GameObject remove;
    public Toggle toggle;

    public TextMeshProUGUI unselectlabel;
    public TextMeshProUGUI selectedLabel;
    public GameObject highlight;
    public Image icon;

    public PracticeModeData data { get; set; }
    public ChallengeMenu menu { get; set; }

    public void Init()
    {
        icon.sprite = data.icon;
        unselectlabel.SetText(data.label);
        selectedLabel.SetText(data.label);
        OnSelect(toggle.isOn);
    }

    void Start()
    {
        toggle.onValueChanged.AddListener(OnSelect);
    }

    void OnSelect(bool selected)
    {
        noselect.SetActive(!selected);
        select.SetActive(selected);
        remove.SetActive(selected);
        highlight.SetActive(selected);
        unselectlabel.gameObject.SetActive(!selected);
        selectedLabel.gameObject.SetActive(selected);

        menu.UpdateScoreMultiply();
    }
}
