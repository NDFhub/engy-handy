using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PracticeItemSelectButton : MonoBehaviour
{
    const int SELECT_WIDTH = 300;
    const int UNSELECT_WIDTH = 180;
    const int SELECT_HIGH = 250;
    const int UNSELECT_HIGH = 160;
    static Color UNSELECT_COLOR = new Color(0.6f, 0.6f, 0.6f, 0.75f);

    LayoutElement layout;
    Toggle toggle;

    public bool is_selected
    {
        get
        {
            return toggle.isOn;
        }
    }
    public GameObject highlight;
    public GameObject selectedLabel;
    public GameObject unselectLabel;
    public GameObject lockIcon;
    public Image icon;

    public PracticeModeData data { set; get; }
    public void Init ()
    {
        icon.sprite = data.icon;
        selectedLabel.GetComponent<TMPro.TextMeshProUGUI>().SetText(data.label);
        unselectLabel.GetComponent<TMPro.TextMeshProUGUI>().SetText(data.label);

        SetLock(false);
    }

    private void OnEnable()
    {
        // update set lock
        SetLock(false);
    }

    void Awake()
    {
        toggle = GetComponent<Toggle>();
        layout = GetComponent<LayoutElement>();

        toggle.onValueChanged.AddListener(OnSelect);

        OnSelect(toggle.isOn);

        toggle.group = transform.parent.GetComponent<ToggleGroup>();
    }

    public void SetLock(bool _lock)
    {
        if (toggle == null)
            toggle = GetComponent<Toggle>();

        lockIcon.SetActive(_lock);
        toggle.interactable = !_lock;
    }

    public void OnSelect(bool selected)
    {
        update_graphics();
    }

    void update_graphics()
    {
        if (layout)
        {
            //layout.preferredWidth = is_selected ? SELECT_WIDTH : UNSELECT_WIDTH;
            //layout.preferredHeight = is_selected ? SELECT_HIGH : UNSELECT_HIGH;

            DOTween.To(() => layout.preferredWidth, w => layout.preferredWidth = w, is_selected ? SELECT_WIDTH : UNSELECT_WIDTH, 0.1f);
            DOTween.To(() => layout.preferredHeight, h => layout.preferredHeight = h, is_selected ? SELECT_HIGH : UNSELECT_HIGH, 0.1f);
        }

        selectedLabel.SetActive(is_selected);
        unselectLabel.SetActive(is_selected == false);
        highlight.SetActive(is_selected);
        icon.color = is_selected ? Color.white : UNSELECT_COLOR;
    }

}
