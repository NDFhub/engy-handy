using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using System.Linq;
using TMPro;


public class ChallengeMenu : MonoBehaviour
{
    public AssetReference prefab;
    public Transform root;
    public Transform dataRoot;
    public TextMeshProUGUI multiplay;
    Button startButton;

    public List<PracticeModeData> list;
    List<Toggle> toggles;

    public PracticeModeData current;
    const string FORMAT = "SCORE X{0}";

    // Start is called before the first frame update
    void Start()
    {
        UIComponent.Get("LOADING").gameObject.SetActive(true);

        list = new List<PracticeModeData>();
        list.AddRange(dataRoot.GetComponentsInChildren<PracticeModeData>());

        int count = 0;

        toggles = new List<Toggle>();
        foreach (var item in list)
        {
            prefab.InstantiateAsync(root).Completed += x =>
            {
                var button = x.Result.GetComponent<ChallengeSelectButton>();
                button.data = item;
                button.menu = this;
                button.Init();
                toggles.Add(button.GetComponent<Toggle>());

                count++;
                if (count == 1)
                {
                    UIComponent.Get("LOADING").gameObject.SetActive(false);
                }
            };
        }
    }

    public void UpdateScoreMultiply()
    {
        var all = toggles.Where(x => x.isOn);
        var count = Mathf.Max(1, all.Count());
        multiplay.SetText(string.Format(FORMAT, count));

        if (startButton == null)
        {
            var sb = UIComponent.Get("STARTCHALLENGE");
            if (sb)
                startButton = sb.GetComponent<Button>();
        }

        var select_items = all.Count() > 0;
        var has_energy = API.singleton.res.energy.currentEnergy >= API.singleton.NeedEnergyToPlay();

        startButton.interactable = select_items && has_energy;
    }

    public void ClickPlay()
    {
        var all = toggles.Where(x => x.isOn);
        var count = Mathf.Max(1, all.Count());
        if (all.Count() > 0)
        {
            var items = new List<PracticeModeData>();
            foreach (var item in all)
            {
                var button = item.GetComponent<ChallengeSelectButton>();
                items.Add(button.data);
            }

            var games = new List<AssetReferenceGameObject>();
            foreach (var item in items)
            {
                games.AddRange(item.games);
            }

            Main.singleton.StartHardGame(games, count);
        }
    }
}
