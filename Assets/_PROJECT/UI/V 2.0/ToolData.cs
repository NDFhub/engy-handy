using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolData : MonoBehaviour
{
    public string id;
    public Sprite icon;
    public AudioClip click;

}
