using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickReplay : MonoBehaviour
{

    void Start()
    {
        var button = GetComponent<Button>();
        if (button)
        {
            button.onClick.AddListener(Click);
        }
    }

    void Click()
    {
        Main.singleton.Replay();
    }

}
