using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SoundButton : MonoBehaviour
{
    public Sprite on, off;
    public Image icon;
    Button button;
    bool is_on;

    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Click);

        is_on = PlayerPrefs.GetInt("sound", 1) > 0 ? true : false;
        Set();
    }

    void Click ()
    {
        is_on = !is_on;
        PlayerPrefs.SetInt("sound", is_on ? 1 : 0);
        Set();
    }

    void Set()
    {
        if (is_on)
        {
            AudioListener.pause = false;
            icon.sprite = on;
            
        }
        else
        {
            AudioListener.pause = true;
            icon.sprite = off;
        }
    }
}
