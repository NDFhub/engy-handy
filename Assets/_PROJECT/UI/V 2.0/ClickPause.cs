using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ClickPause : MonoBehaviour
{
    public enum Action
    {
        Pause, Resume
    }

    public Action action;

    void Start()
    {
        var button = GetComponent<Button>();
        if (action == Action.Pause)
        {
            button.onClick.AddListener(() =>
            {
                var p = UIComponent.Get("PAUSE");
                if (p)
                {
                    p.gameObject.SetActive(true);
                }

                var ui = UIComponent.Get("GAMEPLAY UI");
                if (ui)
                {
                    ui.gameObject.SetActive(false);
                }
            });
        }
        else if (action == Action.Resume)
        {
            button.onClick.AddListener(() =>
            {
                var p = UIComponent.Get("PAUSE");
                if (p)
                {
                    p.gameObject.SetActive(false);
                }

                var ui = UIComponent.Get("GAMEPLAY UI");
                if (ui)
                {
                    ui.gameObject.SetActive(true);
                }
            });
        }
       
    }
}
