using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXControl : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip audioClip;
    public float Volume = 0.8f;
    private void Awake()
    {
        audioSource.PlayOneShot(audioClip, Volume);
    }
    private void Update()
    {
        if(audioSource.isPlaying == false)
        {
            this.gameObject.SetActive(false);
        }
    }
}
