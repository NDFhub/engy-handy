using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
   // public GameObject gameObject;

    [SerializeField] public float CountTime; // Time spend to finish 360 degree

    private float RotationSpeed;
    private float Degree = 360;
   
    void Start()
    {
        RotationSpeed = Degree / CountTime;
    }

   
    void Update()
    {
        RotationSpeed = Degree / CountTime;

        this.transform.Rotate(0, 0, RotationSpeed*Time.deltaTime);
    }
}
