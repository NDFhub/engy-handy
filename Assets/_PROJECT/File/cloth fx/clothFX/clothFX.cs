using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clothFX : MonoBehaviour
{
    new Camera camera;
    void Start()
    {
        camera = Camera.main;
        GameObject.Destroy(gameObject, 1f);
    }

    private void Update()
    {
        transform.rotation = camera.transform.rotation;
    }

}
